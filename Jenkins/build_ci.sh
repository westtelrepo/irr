#!/bin/bash

set -e

cp -rT . /usr/local/src/irr/

CXX=clang++ CXXFLAGS="-Werror -Weverything -Wno-padded -Wno-c++98-compat -Wno-c++98-compat-pedantic -Wno-missing-braces --system-header-prefix=json/" meson /usr/local/src/irr/server/ /usr/local/src/irr_build/
pushd /usr/local/src/irr_build/
ninja irr irr_test
popd
/usr/local/src/irr_build/irr_test --reporter junit --out junit-ci.xml
cp /usr/local/src/irr_build/irr irr-$(lsb_release -cs)-$(dpkg --print-architecture)-$(/usr/local/src/irr_build/irr --semver)
cp /usr/local/src/irr_build/irr_test irr_test
