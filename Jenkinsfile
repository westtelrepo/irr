pipeline {
    agent none
    stages {
        stage('Build') {
            parallel {
                stage('Build on stretch/amd64') {
                    agent {
                        dockerfile { filename 'Jenkins/build/Dockerfile.stretch' }
                    }
                    steps {
                        sh './Jenkins/build_ci.sh'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        success {
                            archiveArtifacts artifacts: 'irr-*', fingerprint: true
                            stash name: 'amd64', includes: 'irr-*,irr_test'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Build on stretch/i386') {
                    agent {
                        dockerfile { filename 'Jenkins/build/Dockerfile.stretch.i386' }
                    }
                    steps {
                        sh './Jenkins/build_ci.sh'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        success {
                            archiveArtifacts artifacts: 'irr-*', fingerprint: true
                            stash name: 'i386', includes: 'irr-*,irr_test'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
            }
        }
        stage('Deploy Test') {
            parallel {
                stage('Deploy on stretch/amd64') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.stretch' }
                    }
                    steps {
                        unstash name: 'amd64'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Deploy on stretch/i386') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.stretch.i386' }
                    }
                    steps {
                        unstash name: 'i386'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Deploy on jessie/amd64') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.jessie' }
                    }
                    steps {
                        unstash name: 'amd64'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Deploy on jessie/i386') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.jessie.i386' }
                    }
                    steps {
                        unstash name: 'i386'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Deploy on buster/amd64') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.buster' }
                    }
                    steps {
                        unstash name: 'amd64'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
                stage('Deploy on buster/i386') {
                    agent {
                        dockerfile { filename 'Jenkins/deploy/Dockerfile.buster.i386' }
                    }
                    steps {
                        unstash name: 'i386'
                        // a simple "can we run this executable"
                        sh './irr-* --version'
                        sh './irr_test --reporter junit --out junit-ci.xml'
                    }
                    post {
                        always {
                            junit 'junit-ci.xml'
                        }
                        cleanup {
                            cleanWs()
                        }
                    }
                }
            }
        }
    }
}
