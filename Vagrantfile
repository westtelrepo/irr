# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  config.vm.box = "debian/contrib-stretch64"
  # disable the vagrant-vbguest plugin, doesn't work for some reason
  config.vbguest.auto_update = false

  #config.vm.provision "ansible_local" do |ansible|
  #  ansible.playbook = "latest_ansible.yml"
  #end

  config.vm.provision "shell", inline: <<-SHELL
    if [ ! -f /etc/first_run_done ]; then
      export DEBIAN_FRONTEND=noninteractive
      apt-get update
      apt-get -y install dirmngr
      echo deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main >> /etc/apt/sources.list
      apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
      apt-get update
      apt-get -y install ansible
      touch /etc/first_run_done
    fi
  SHELL

  config.vm.define "irr", primary: true do |irr|
    # enable audio and audio controller
    irr.vm.provider "virtualbox" do |vb|
      if Vagrant::Util::Platform.windows?
        vb.customize ["modifyvm", :id, '--audio', 'dsound', '--audiocontroller', 'ac97']
      elsif Vagrant::Util::Platform.linux?
        vb.customize ["modifyvm", :id, '--audio', 'pulse', '--audiocontroller', 'ac97']
      end
      vb.customize ["modifyvm", :id, '--audioout', 'on']
      vb.memory = 1024
      vb.cpus = 4
    end

    irr.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "common.yml"
    end

    irr.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "default.yml"
    end

    irr.vm.network "private_network", ip: "172.16.5.20", virtualbox__intnet: true
  end

  config.vm.define "irr32", autostart: false do |irr|
    irr.vm.box = "bento/debian-9.4-i386"
    # enable audio and audio controller
    irr.vm.provider "virtualbox" do |vb|
      if Vagrant::Util::Platform.windows?
        vb.customize ["modifyvm", :id, '--audio', 'dsound', '--audiocontroller', 'ac97']
      elsif Vagrant::Util::Platform.linux?
        vb.customize ["modifyvm", :id, '--audio', 'pulse', '--audiocontroller', 'ac97']
      end
      vb.customize ["modifyvm", :id, '--audioout', 'on']
      vb.memory = 1024
      vb.cpus = 4
    end

    irr.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "common.yml"
    end

    irr.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "default.yml"
    end

    irr.vm.network "private_network", ip: "172.16.5.22", virtualbox__intnet: true
  end

  config.vm.define "db" do |db|
    db.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "common.yml"
    end

    db.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "db.yml"
    end

    db.vm.network "private_network", ip: "172.16.5.21", virtualbox__intnet: true
  end

  config.vm.define "jessie_test", autostart: false do |jessie_test|
    jessie_test.vm.box = "debian/contrib-jessie64"
    jessie_test.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "common.yml"
    end
    jessie_test.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "only_irr.yml"
    end
    jessie_test.vm.network "private_network", ip: "172.16.5.23", virtualbox__intnet: true
  end

  config.vm.define "jessie_test32", autostart: false do |jessie_test|
    jessie_test.vm.box = "bento/debian-8.10-i386"
    jessie_test.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "common.yml"
    end
    jessie_test.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "only_irr.yml"
    end
    jessie_test.vm.network "private_network", ip: "172.16.5.24", virtualbox__intnet: true
  end

  config.vm.define "jenkins", autostart: false do |jenkins|
    jenkins.vm.box = "debian/contrib-stretch64"
    jenkins.vm.hostname = "jenkins"
    jenkins.vm.network "forwarded_port", guest: 8080, host: 8080, host_ip: "127.0.0.1"

    # don't allow jenkins to write to /vagrant
    jenkins.vm.synced_folder ".", "/vagrant", owner: "vagrant", group: "vagrant", mount_options: ["dmode=775,fmode=664"]

    jenkins.vm.provider "virtualbox" do |vb|
      vb.memory = 4096
    end

    jenkins.vm.provision "ansible_local" do |ansible|
      ansible.playbook = "Jenkins/jenkins.yml"
    end
  end
end
