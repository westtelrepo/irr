#ifndef WESTTEL_OPUS
#define WESTTEL_OPUS
#include <opus/opus.h>
#include <iostream>

namespace opus {
	class Encoder {
	private:
		OpusEncoder* encoder_;
	public:
		Encoder() : encoder_{nullptr} { }

		Encoder(const Encoder&) = delete;

		Encoder(Encoder&& e) noexcept: encoder_{e.encoder_} {
			e.encoder_ = nullptr;
		}

		// the Encoder argument is not missing a &, this is on purpose
		// http://en.cppreference.com/w/cpp/language/operators#Assignment_operator
		Encoder& operator=(Encoder e) noexcept {
			std::swap(encoder_, e.encoder_);
			return *this;
		}

		Encoder(int32_t Fs, int channels, int application) {
			int error = 0;
			encoder_ = opus_encoder_create(Fs, channels, application, &error);
			if (encoder_ == nullptr) {
				throw std::runtime_error{std::string{"Could not create Opus encoder: "} + opus_strerror(error)};
			}
		}

		~Encoder() {
			opus_encoder_destroy(encoder_);
		}

		size_t encode(const int16_t* pcm, int frame_size, void* data, size_t data_len) {
			int ret = opus_encode(encoder_, pcm, frame_size,
				static_cast<unsigned char*>(data), static_cast<opus_int32>(data_len));
			if (ret < 0) {
				throw std::runtime_error{std::string{"Could not encode: "} + opus_strerror(ret)};
			}

			return static_cast<size_t>(ret);
		}

		int32_t bitrate() {
			opus_int32 ret = 0;
			int e = opus_encoder_ctl(encoder_, OPUS_GET_BITRATE(&ret));
			if (e) {
				throw std::runtime_error{std::string{"Could not get bitrate: "} + opus_strerror(e)};
			}
			return ret;
		}

		void bitrate(int32_t b) {
			int e = opus_encoder_ctl(encoder_, OPUS_SET_BITRATE(b));
			if (e) {
				throw std::runtime_error{std::string{"Could not set bitrate: "} + opus_strerror(e)};
			}
		}

		int32_t lookahead() {
			opus_int32 ret = 0;
			int e = opus_encoder_ctl(encoder_, OPUS_GET_LOOKAHEAD(&ret));
			if (e) {
				throw std::runtime_error{std::string{"Could not get Opus lookahead: "} + opus_strerror(e)};
			}
			return ret;
		}

		void dtx(bool enabled) {
			int e = opus_encoder_ctl(encoder_, OPUS_SET_DTX(static_cast<opus_int32>(enabled)));
			if (e) {
				throw std::runtime_error{std::string{"Could not set Opus DTX: "} + opus_strerror(e)};
			}
		}

		void lsb_depth(int32_t bits) {
			int e = opus_encoder_ctl(encoder_, OPUS_SET_LSB_DEPTH(bits));
			if (e) {
				throw std::runtime_error{std::string{"Could not set Opus LSB Depth: "} + opus_strerror(e)};
			}
		}

		int32_t complexity() {
			opus_int32 ret = 0;
			int e = opus_encoder_ctl(encoder_, OPUS_GET_COMPLEXITY(&ret));
			if (e) {
				throw std::runtime_error{std::string{"Could not get Opus complexity: "} + opus_strerror(e)};
			}
			return ret;
		}

		void complexity(int32_t c) {
			int e = opus_encoder_ctl(encoder_, OPUS_SET_COMPLEXITY(c));
			if (e) {
				throw std::runtime_error{std::string{"Could not set Opus complexity: "} + opus_strerror(e)};
			}
		}

		explicit operator bool() { return encoder_; }
	};

	class Decoder {
	private:
		OpusDecoder* decoder_;
	public:
		Decoder(const Decoder&) = delete;
		Decoder& operator=(const Decoder&) = delete;

		Decoder(Decoder&& d) noexcept: decoder_{d.decoder_} {
			d.decoder_ = nullptr;
		}

		Decoder(int32_t Fs, int channels) {
			int error = 0;
			decoder_ = opus_decoder_create(Fs, channels, &error);
			if (decoder_ == nullptr) {
				throw std::runtime_error{std::string{"Could not create Opus decoder: "} + opus_strerror(error)};
			}
		}

		~Decoder() {
			opus_decoder_destroy(decoder_);
		}

		size_t decode(const void* data, size_t len, int16_t* pcm, int frame_size, bool decode_fec=false) {
			int ret = opus_decode(decoder_, static_cast<const unsigned char*>(data),
				static_cast<opus_int32>(len), pcm, frame_size, decode_fec);
			if (ret < 0) {
				throw std::runtime_error{std::string{"Could not decode Opus packet: "} + opus_strerror(ret)};
			}

			return static_cast<size_t>(ret);
		}

		explicit operator bool() { return decoder_; }
	};
}

#endif
