#ifndef WESTTEL_PCM
#define WESTTEL_PCM

#include <cassert>

enum PCMformat {
	pcm_int16,
};

class PCM {
private:
	PCMformat format_;
	int64_t freq_;

	// TODO: channels?
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-private-field"
	int channels_;
#pragma clang diagnostic pop
	size_t samples_;
	std::unique_ptr<char[]> data_;
public:
	PCM(PCMformat format, int64_t freq, int channels, size_t samples, std::unique_ptr<char[]>&& data)
	: format_{format}, freq_{freq}, channels_{channels}, samples_{samples}, data_{std::move(data)} {
	}

	int16_t* i16data() {
		assert(format() == pcm_int16);
		return reinterpret_cast<int16_t*>(data_.get());
	}

	int64_t freq() { return freq_; }
	PCMformat format() { return format_; }
	size_t samples() { return samples_; }
};

#endif
