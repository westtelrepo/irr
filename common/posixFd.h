#ifndef WESTTEL_POSIX_FD
#define WESTTEL_POSIX_FD

#include <fcntl.h>
#include <utility>

class posixFd {
private:
	int fd_;
public:
	posixFd(): fd_{-1} {}

	posixFd(const posixFd&) = delete;
	posixFd(posixFd&& p): fd_{p.fd_} { p.fd_ = -1; }

	posixFd& operator=(const posixFd&) = delete;
	posixFd& operator=(posixFd&& p) {
		std::swap(fd_, p.fd_);
		return *this;
	}

	// take ownership of fd
	posixFd(int fd): fd_{fd} {}

	off_t lseek(off_t offset, int whence) {
		off_t ret = ::lseek(fd_, offset, whence);
		if (ret == -1) {
			// TODO: replace with exception
			perror("Could not seek");
			exit(1);
		}
		return ret;
	}

	size_t read(void* buf, size_t count) {
		assert(count <= SSIZE_MAX);

		while (true) {
			ssize_t ret = ::read(fd_, buf, count);
			if (ret == -1 && errno == EINTR)
				continue;
			else if (ret == -1) {
				// TODO: replace with exception
				perror("Can not read file");
				exit(1);
			}

			return static_cast<size_t>(ret);
		}
	}

	template<size_t N>
	size_t read(std::array<char, N>& buf) {
		return this->read(buf.data(), buf.size());
	}

	~posixFd() {
		if (fd_ == -1)
			return;
		// close can create an error, but there is nothing that can be done here
		// Linux guarantees the file descriptor will be closed at least
		// DO NOT retry on EINTR under Linux, see `man 2 close'
		errno = 0;
		int e = close(fd_);
		if (e == -1) {
			// TODO: log error
			perror("Could not close file");
		}
	}

	static posixFd open(const char* pathname, int flags) {
		while (true) {
			errno = 0;
			int fd = ::open(pathname, flags);
			if (fd == -1 && errno == EINTR)
				continue;
			else if (fd == -1) {
				// TODO: replace with exception
				perror("Can not open file");
				exit(1);
			}

			return posixFd{fd};
		}
	}

};

class saveSeek {
private:
	posixFd& fd_;
	off_t save_;
public:
	saveSeek(posixFd& fd): fd_{fd} {
		save_ = fd_.lseek(0, SEEK_CUR);
	}

	~saveSeek() {
		fd_.lseek(save_, SEEK_SET);
	}

};

#endif
