#ifndef WESTTEL_TCP_FRAMES
#define WESTTEL_TCP_FRAMES

#include <boost/asio.hpp>

class tcp_frames {
private:
	boost::asio::ip::tcp::socket socket_;
	boost::asio::streambuf inbuf_;
	uint16_t len16_;
public:
	tcp_frames(boost::asio::ip::tcp::socket&& socket): socket_{std::move(socket)} { }

	template<class F>
	void async_read(F f) {
		boost::asio::async_read(socket_, inbuf_, boost::asio::transfer_exactly(2),
		[this, f] (const boost::system::error_code& e1, size_t) {
			if (e1) {
				f(e1, std::vector<char>{});
				return;
			}

			uint16_t datalen = 0;
			{
				std::istream is{&inbuf_};
				is.read(reinterpret_cast<char*>(&datalen), sizeof(uint16_t));
			}

			boost::asio::async_read(socket_, inbuf_, boost::asio::transfer_exactly(datalen),
			[this, f, datalen] (const boost::system::error_code& e2, size_t) {
				if (e2) {
					f(e2, std::vector<char>{});
					return;
				}

				std::vector<char> data;
				data.resize(datalen);

				std::istream is{&inbuf_};
				is.read(data.data(), static_cast<std::streamsize>(data.size()));

				f(boost::system::error_code{}, std::move(data));
			});
		});
	}

	template<class F>
	void async_write(const void* buf, const size_t len, F f) {
		assert(len < UINT16_MAX);
		len16_ = static_cast<uint16_t>(len);

		std::array<boost::asio::const_buffer, 2> bufs = {
 			boost::asio::buffer(reinterpret_cast<char*>(&len16_), sizeof(len16_)),
			boost::asio::buffer(buf, len)
		};

		boost::asio::async_write(socket_, bufs, [this, f] (const boost::system::error_code& e, size_t) {
			f(e);
		});
	}

	template<class F>
	void async_write_many(const std::vector<std::string>& bufs, F f) {
		std::vector<boost::asio::const_buffer> write;
		auto len16s = std::make_shared<std::vector<uint16_t>>(bufs.size());
		write.reserve(bufs.size() * 2);

		for (size_t i = 0; i < bufs.size(); i++) {
			assert(bufs[i].size() < UINT16_MAX);
			(*len16s)[i] = static_cast<uint16_t>(bufs[i].size());

			write.push_back(boost::asio::buffer(reinterpret_cast<char*>(len16s->data() + i), sizeof(uint16_t)));
			write.push_back(boost::asio::buffer(bufs[i].data(), bufs[i].size()));
		}

		boost::asio::async_write(socket_, write, [this, f, len16s] (const boost::system::error_code& e, size_t) {
			f(e);
		});
	}

	// prints remote_ip:remote_port local_ip:local_port
	std::string connection_details() const {
		try {
			std::ostringstream ss;
			auto remote = socket_.remote_endpoint().address();
			auto local = socket_.local_endpoint().address();

			// print brackets around v6 address, aka [::1]:6000
			if (remote.is_v6())
				ss << "[" << remote.to_string() << "]";
			else
				ss << remote.to_string();
			ss << ":" << socket_.remote_endpoint().port();

			ss << " ";

			if (local.is_v6())
				ss << "[" << local.to_string() << "]";
			else
				ss << local.to_string();
			ss << ":" << socket_.local_endpoint().port();
			return ss.str();
		} catch(const std::exception&) {
			return "unknown endpoint info";
		}
	}
};

#endif
