#include "config.h"

#include <yaml-cpp/yaml.h>

#include <iostream>
#include <mutex>

irr_config::irr_config(std::string file_contents) {
    YAML::Node node = YAML::Load(file_contents);

    if (const char* str = getenv("IRR_RECORDING_DIR"))
        recording_dir_ = str;

    if (const char* str = getenv("IRR_RETENTION_LIMIT"))
        retention_limit_ = static_cast<size_t>(std::stoull(str));

    if (const char* str = getenv("IRR_RETENTION_INTERVAL"))
        retention_interval_ = std::chrono::seconds(std::stoull(str));

    if (const char* str = getenv("IRR_PORT"))
        port_ = static_cast<unsigned short>(std::stoi(str));

    if (YAML::Node alerts = node["alerts"]) {
        if (auto active = alerts["active"])
            alerts_active_ = active.as<bool>();
        if (auto email = alerts["email"]) {
            if (auto to = email["to"]) {
                if (!to.IsSequence())
                    throw std::runtime_error{"alerts.email.to must be a string sequence"};
                for (const auto& e: to)
                    email_recipients_.push_back(e.as<std::string>());
            }

            if (auto server = email["server"])
                email_server_ = server.as<std::string>();

            if (auto from = email["from"]) {
                if (auto address = from["address"])
                    email_from_address_ = address.as<std::string>();
                if (auto phrase = from["phrase"])
                    email_from_phrase_ = phrase.as<std::string>();
            }

            if (auto startup = email["startup"])
                email_on_startup_ = startup.as<bool>();
            if (auto reload = email["reload"])
                email_on_reload_ = reload.as<bool>();

            if (auto warning_escalate_sec = email["warning_escalate_sec"])
                warning_escalate_sec_ = warning_escalate_sec.as<time_t>();
            if (auto reminder_sec = email["reminder_sec"])
                reminder_sec_ = reminder_sec.as<time_t>();
            if (auto all_clear_sec = email["all_clear_sec"])
                all_clear_sec_ = all_clear_sec.as<time_t>();
        }
    }

    if (auto recordings = node["recordings"]) {
        if (auto dir = recordings["dir"])
            recording_dir_ = dir.as<std::string>();

        if (auto opus_complexity = recordings["opus_complexity"]) {
            int complexity = opus_complexity.as<int>();
            if (complexity < 0 || complexity > 10) {
                throw std::runtime_error{"Opus complexity must be between 0 and 10 inclusive"};
            }
            opus_complexity_ = complexity;
        }

        if (auto assume_death_seconds = recordings["assume_death_seconds"]) {
            assume_death_seconds_ = assume_death_seconds.as<unsigned int>();
        }
    }

    if (auto db = node["db"]) {
        if (auto retention = db["retention"]) {
            if (auto limit = retention["limit"])
                retention_limit_ = limit.as<size_t>();
            if (auto interval = retention["interval"])
                retention_interval_ = std::chrono::seconds(interval.as<size_t>());
            if (auto seconds = retention["age_seconds"])
                retention_age_seconds_ = seconds.as<size_t>();
            if (auto cron = retention["cron"]) {
                for (auto e: cron) {
                    cron_config c;
                    c.expr = cron::make_cron(e["expr"].as<std::string>());
                    c.age = e["age"].as<size_t>();
                    cron_config_.push_back(c);
                }
            }
        }

        if (auto uri = db["uri"]) {
            db_uri_ = uri.as<std::string>();
        }
    }

    if (auto port = node["port"]) {
        port_ = port.as<unsigned short>();
    }

    if (auto rtp = node["rtp"]) {
        if (auto psap_toolkit = rtp["psap_toolkit"]) {
            if (auto address = psap_toolkit["address"])
                ptk_address_ = address.as<std::string>();
            if (auto port = psap_toolkit["port"])
                ptk_port_ = port.as<unsigned short>();
            if (auto position = psap_toolkit["position"])
                ptk_pos_ = position.as<int>();
            if (auto ignore_on_hold = psap_toolkit["ignore_on_hold"])
                ptk_ignore_on_hold_ = ignore_on_hold.as<bool>();
        }

        if (auto endpoints = rtp["endpoints"]) {
            for (auto endpoint: endpoints) {
                rtp_endpoint_config cfg;
                cfg.address = boost::asio::ip::address_v4::from_string(endpoint["address"].as<std::string>());
                cfg.port = endpoint["port"].as<unsigned short>();
                cfg.position = endpoint["position"].as<int>();
                cfg.format = rtp_format_from_string(endpoint["format"].as<std::string>());

                if (auto volume = endpoint["volume"]) {
                    cfg.volume = volume.as<double>();
                }

                if (rtp_endpoints_.count(cfg.position)) {
                    throw std::runtime_error{"Config may not have two endpoints for a position"};
                }
                rtp_endpoints_[cfg.position] = cfg;
            }
        }

        if (auto comfort = rtp["comfort"]) {
            if (auto level = comfort["level"]) {
                rtp_comfort_ = level.as<double>();
            }

            if (auto detection_level = comfort["detection_level"]) {
                rtp_detection_level_ = detection_level.as<double>();
            }
        }

        if (auto always_play = rtp["always_play"]) {
            rtp_always_play_ = always_play.as<bool>();
        }

        if (auto offset = rtp["offset"]) {
            rtp_offset_ = offset.as<double>();
        }

        if (auto position_uncertainty = rtp["position_uncertainty"]) {
            rtp_position_uncertainty_ = position_uncertainty.as<double>();
        }
    }

    if (auto log = node["log"]) {
        if (auto level = log["level"]) {
            log_level_ = level.as<int>();
        }

        if (auto destination = log["destination"]) {
            std::string dest = destination.as<std::string>();
            if (dest == "stdout") {
                log_dest_ = log_dest::Stdout;
            } else if (dest == "systemd") {
                log_dest_ = log_dest::systemd;
            } else {
                throw new std::runtime_error{"Invalid log destination"};
            }
        }

        if (auto ptk_xml = log["ptk_xml"]) {
            log_ptk_xml_ = ptk_xml.as<bool>();
        }

        if (auto missed_rtp_frames = log["missed_rtp_frames"]) {
            log_missed_frames_ = missed_rtp_frames.as<size_t>();
        }

        if (auto rtp_thread_lock_sec = log["rtp_thread_lock_sec"]) {
            rtp_thread_lock_sec_ = rtp_thread_lock_sec.as<double>();
        }

        if (auto rtp_ran_for_sec = log["rtp_ran_for_sec"]) {
            rtp_ran_for_sec_ = rtp_ran_for_sec.as<double>();
        }
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static std::list<std::weak_ptr<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>>> funcs;
#pragma clang diagnostic pop
static std::mutex funcs_mutex;

void config_subscribe(std::weak_ptr<std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)>> f) {
    std::lock_guard<std::mutex> guard{funcs_mutex};
    funcs.emplace_back(std::move(f));
}

static void config_notify(std::shared_ptr<const irr_config> old, std::shared_ptr<const irr_config> new_config) {
    std::lock_guard<std::mutex> guard{funcs_mutex};
    for(auto i = funcs.begin(); i != funcs.end();) {
        if (auto f = i->lock()) {
            (*f)(old, new_config);
            ++i;
        } else {
            i = funcs.erase(i);
        }
    }
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static std::shared_ptr<const irr_config> irr_config_int;
#pragma clang diagnostic pop

void config_set(std::shared_ptr<const irr_config> new_config) {
    auto old = std::atomic_load(&irr_config_int);
    std::atomic_store(&irr_config_int, new_config);
    config_notify(old, new_config);
}

std::shared_ptr<const irr_config> config_get() {
    return std::atomic_load(&irr_config_int);
}
