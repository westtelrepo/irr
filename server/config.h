#ifndef IRR_CONFIG
#define IRR_CONFIG

#include <croncpp.h>
#include <boost/asio.hpp>
#include <boost/optional.hpp>
#include <chrono>
#include <map>
#include <memory>
#include <string>
#include <vector>

enum class rtp_format {
    BARIX_PCM_8kHZ_104,
    ULAW_8kHZ_0,
    ALAW_8kHZ_8
};

inline rtp_format rtp_format_from_string(const std::string& s) {
    if (s == "BARIX_PCM_8kHZ_104")
        return rtp_format::BARIX_PCM_8kHZ_104;
    else if (s == "ULAW_8kHZ_0")
        return rtp_format::ULAW_8kHZ_0;
    else if (s == "ALAW_8kHZ_8")
        return rtp_format::ALAW_8kHZ_8;
    else
        throw std::runtime_error{s + " is not a valid rtp_format"};
}

struct rtp_endpoint_config {
    boost::asio::ip::address_v4 address;
    unsigned short port;
    int position;
    rtp_format format;
    double volume = 1;
};

enum class log_dest {
    Stdout,
    systemd
};

struct cron_config {
    cron::cronexpr expr;
    size_t age;
};

class irr_config {
private:
    bool alerts_active_ = false;
    bool email_on_startup_ = true;
    bool email_on_reload_ = true;
    time_t warning_escalate_sec_ = 15;
    time_t reminder_sec_ = 3600;
    time_t all_clear_sec_ = 60;

    std::vector<std::string> email_recipients_;
    std::string email_server_ = "localhost:25";
    std::string email_from_phrase_ = "IRR Testing";
    std::string email_from_address_ = "irr@example.com";

    std::string recording_dir_ = "/var/opt/irr_recordings";
    unsigned int assume_death_seconds_ = 120;

    std::chrono::seconds retention_interval_ = std::chrono::seconds(3600);
    size_t retention_limit_ = 500;
    boost::optional<size_t> retention_age_seconds_;

    std::string db_uri_ = "";

    unsigned short port_ = 6000;

    boost::optional<std::string> ptk_address_;
    boost::optional<unsigned short> ptk_port_;
    boost::optional<int> ptk_pos_;
    bool ptk_ignore_on_hold_ = false;

    std::map<int, rtp_endpoint_config> rtp_endpoints_;
    double rtp_comfort_ = 0.0;
    boost::optional<double> rtp_detection_level_;
    bool rtp_always_play_ = false;
    double rtp_offset_ = 1.2;
    double rtp_position_uncertainty_ = 0;

    int log_level_ = 7;
    log_dest log_dest_ = log_dest::Stdout;
    bool log_ptk_xml_ = false;
    size_t log_missed_frames_ = 20;
    double rtp_thread_lock_sec_ = 0.0001;
    double rtp_ran_for_sec_ = 0.01;

    int opus_complexity_ = 9;

    std::vector<cron_config> cron_config_;
public:
    irr_config(): irr_config("") { }
    irr_config(std::string filename);

    bool alerts_active() const { return alerts_active_; }
    bool email_startup() const { return alerts_active_ && email_on_startup_; }
    bool email_reload() const { return alerts_active_ && email_on_reload_; }

    std::vector<std::string> email_recipients() const { return email_recipients_; }
    std::string email_server() const { return email_server_; }
    std::string email_from_phrase() const { return email_from_phrase_; }
    std::string email_from_address() const { return email_from_address_; }

    std::string recording_dir() const { return recording_dir_; }
    unsigned int assume_death_seconds() const { return assume_death_seconds_; }

    time_t warning_escalate_sec() const { return warning_escalate_sec_; }
    time_t reminder_sec() const { return reminder_sec_; }
    time_t all_clear_sec() const { return all_clear_sec_; }

    std::chrono::seconds retention_interval() const { return retention_interval_; }
    size_t retention_limit() const { return retention_limit_; }
    boost::optional<size_t> retention_age_seconds() const { return retention_age_seconds_; }

    std::string db_uri() const { return db_uri_; }

    unsigned short port() const { return port_; }

    std::vector<cron_config> cron() const { return cron_config_; }

    boost::optional<std::string> ptk_address() const { return ptk_address_; }
    boost::optional<unsigned short> ptk_port() const { return ptk_port_; }
    boost::optional<int> ptk_position() const { return ptk_pos_; }
    bool ptk_ignore_on_hold() const { return ptk_ignore_on_hold_ ; }

    std::map<int, rtp_endpoint_config> rtp_endpoints() const { return rtp_endpoints_; }
    double rtp_comfort_level() const { return rtp_comfort_; }
    boost::optional<double> rtp_detection_level() const { return rtp_detection_level_; }
    bool rtp_always_play() const { return rtp_always_play_; }
    double rtp_offset() const { return rtp_offset_; }
    double rtp_position_uncertainty() const { return rtp_position_uncertainty_; }

    int log_level() const { return log_level_; }
    log_dest log_dest() const { return log_dest_; }
    bool log_ptk_xml() const { return log_ptk_xml_; }
    size_t log_missed_frames() const { return log_missed_frames_; }
    double rtp_thread_lock_sec() const { return rtp_thread_lock_sec_; }
    double rtp_ran_for_sec() const { return rtp_ran_for_sec_; }

    int opus_complexity() const { return opus_complexity_; }
};

void config_subscribe(std::weak_ptr<std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)>>);

void config_set(std::shared_ptr<const irr_config>);
std::shared_ptr<const irr_config> config_get();

#endif
