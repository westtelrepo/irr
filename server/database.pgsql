--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: irr; Type: DATABASE; Schema: -; Owner: irr
--

CREATE DATABASE irr WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE irr OWNER TO irr;

\connect irr

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: pieces; Type: TABLE; Schema: public; Owner: irr
--

CREATE TABLE pieces (
    streamid text NOT NULL,
    piecenum bigint NOT NULL,
    data bytea NOT NULL
);


ALTER TABLE pieces OWNER TO irr;

--
-- Name: streams; Type: TABLE; Schema: public; Owner: irr
--

CREATE TABLE streams (
    streamid text NOT NULL,
    last_len smallint DEFAULT 0 NOT NULL,
    current boolean DEFAULT true NOT NULL,
    unix_time timestamp with time zone DEFAULT now() NOT NULL,
    callid text
);


ALTER TABLE streams OWNER TO irr;

--
-- Name: pieces pieces_pkey; Type: CONSTRAINT; Schema: public; Owner: irr
--

ALTER TABLE ONLY pieces
    ADD CONSTRAINT pieces_pkey PRIMARY KEY (streamid, piecenum);


--
-- Name: streams streams_pkey; Type: CONSTRAINT; Schema: public; Owner: irr
--

ALTER TABLE ONLY streams
    ADD CONSTRAINT streams_pkey PRIMARY KEY (streamid);


--
-- Name: streams_callid_idx; Type: INDEX; Schema: public; Owner: irr
--

CREATE INDEX streams_callid_idx ON streams USING btree (callid);


--
-- Name: streams_unix_time_idx; Type: INDEX; Schema: public; Owner: irr
--

CREATE INDEX streams_unix_time_idx ON streams USING btree (unix_time);


--
-- Name: pieces pieces_streamid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: irr
--

ALTER TABLE ONLY pieces
    ADD CONSTRAINT pieces_streamid_fkey FOREIGN KEY (streamid) REFERENCES streams(streamid) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

