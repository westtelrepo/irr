#ifndef WESTTEL_LIBESMTP_WRAPPER
#define WESTTEL_LIBESMTP_WRAPPER

#include <array>
#include <cassert>
#include <libesmtp.h>
#include <string>
#include <utility>
#include <cstring>

#include "logger.h"

namespace esmtp {
    // this class must not be used after the session it came from is destroyed
    class message {
    private:
        smtp_message_t message_;
    public:
        message(): message_{nullptr} { }
        message(smtp_message_t m): message_{m} { }

        // assume that every const char* argument must remain alive after the session is done
        void add_recipient(const char* email) {
            smtp_recipient_t e = smtp_add_recipient(message_, email);
            assert(e);
        }

        // argument MUST say alive until after the session is done with
        void set_message(const char* msg) {
            int e = smtp_set_message_str(message_, static_cast<void*>(const_cast<char*>(msg)));
            assert(e);
        }

        // same for this guy (I assume)
        void set_subject(const char* subj) {
            int e = smtp_set_header(message_, "Subject", subj);
            assert(e);
        }

        void set_from(const char* phrase, const char* mailbox) {
            int e = smtp_set_header(message_, "From", phrase, mailbox);
            assert(e);
        }

        static std::string body_to_message(const std::string& body) {
            std::string real_body = "\r\n\r\n";
            real_body.reserve(2 * body.size()); // probably overkill
            for (char c: body) {
                // Add a CR before every LF
                if (c == '\n')
                    real_body += '\r';
                // throw away CR in case it was included
                if (c != '\r')
                    real_body += c;
            }
            return real_body;
        }
    };

    class session {
    private:
        smtp_session_t session_;
    public:
        session() {
            session_ = smtp_create_session();
            assert(session_);
        }

        session(const session&) = delete;
        session(session&& s) noexcept {
            session_ = s.session_;
            s.session_ = nullptr;
        }

        session& operator=(const session&) = delete;
        session& operator=(session& other) {
            std::swap(session_, other.session_);
            return *this;
        }

        ~session() {
            if (session_)
                smtp_destroy_session(session_);
        }

        void set_server(const char* str) {
            int e = smtp_set_server(session_, str);
            assert(e);
        }

        message add_message() {
            smtp_message_t ret = smtp_add_message(session_);
            assert(ret);
            return ret;
        }

        void start() {
            int e = smtp_start_session(session_);
            if (!e) {
                std::array<char, 128> error_string;
                int err = smtp_errno();
                smtp_strerror(err, error_string.data(), error_string.size());

                throw std::runtime_error{error_string.data()};
            }
        }
    };
}

#endif
