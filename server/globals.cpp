#include "globals.h"

std::atomic<int> global_graceful_shutdown{0};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
std::shared_ptr<irr_status> global_irr_status;
#pragma clang diagnostic pop
