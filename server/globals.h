#ifndef WESTTEL_IRR_GLOBALS
#define WESTTEL_IRR_GLOBALS

#include <atomic>
#include <memory>

#include "status.h"

// 0 is do nothing, 1 is graceful shutdown, 2 is less graceful shutdown
extern std::atomic<int> global_graceful_shutdown;

// this variable is set ONLY ONCE in main and then is never set thereafter
// making it usable from multiple threads
extern std::shared_ptr<irr_status> global_irr_status;

#endif
