#include "irr_data.h"

#include <condition_variable>
#include <json/json.h>
#include <map>
#include <mutex>
#include <pqxx/pqxx>
#include <queue>
#include <set>
#include <thread>

#include "config.h"
#include "globals.h"
#include "logger.h"

// this is a transactor for idempotent operations, i.e.
// IF IT DOESN'T KNOW IF IT COMMITTED, IT WILL RETRY UNCONDITIONALLY
template<class F>
auto transactor(F f, std::string description) -> decltype(f()) {
	while(true) {
		try {
			decltype(std::declval<F>()()) ret = f();
			global_irr_status->clear("db");
			return ret;
		} catch (const pqxx::in_doubt_error& e) {
			global_irr_status->set_alarm("db", alarm_priority::warning, "Broken database connection", std::string{"exception: "} + e.what() + "\ndescription: " + description);
			sleep(1);
		} catch (const pqxx::broken_connection& e) {
			global_irr_status->set_alarm("db", alarm_priority::warning, "Broken database connection", std::string{"exception: "} + e.what() + "\ndescription: " + description);
			sleep(1);
		} catch (const pqxx::insufficient_resources& e) {
			global_irr_status->set_alarm("db", alarm_priority::error, "Database is having resource issues",  std::string{"exception: "} + e.what() + "\ndescription: " + description);
			sleep(30);
		} catch (const pqxx::pqxx_exception& e) {
			global_irr_status->set_alarm("db", alarm_priority::programming_error, "Other/unhandled database exception",  std::string{"exception: "} + e.base().what() + "\ndescription: " + description);
			sleep(30);
		}
	}
}

template<class F>
void transactor_once(F f, std::string description) {
	try {
		f();
		global_irr_status->clear("db2");
	} catch (const pqxx::in_doubt_error& e) {
		global_irr_status->set_alarm("db2", alarm_priority::warning, "Broken database connection", std::string{"exception: "} + e.what() + "\ndescription: " + description);
	} catch (const pqxx::broken_connection& e) {
		global_irr_status->set_alarm("db2", alarm_priority::warning, "Broken database connection", std::string{"exception: "} + e.what() + "\ndescription: " + description);
	} catch (const pqxx::insufficient_resources& e) {
		global_irr_status->set_alarm("db2", alarm_priority::error, "Database is having resource issues", std::string{"exception: "} + e.what() + "\ndescription: " + description);
	} catch (const pqxx::pqxx_exception& e) {
		global_irr_status->set_alarm("db2", alarm_priority::programming_error, "Other/unhandled database exception", std::string{"exception: "} + e.base().what() + "\ndescription: " + description);
	}
}

struct PutWork {
	enum WorkType {
		EndStream,
		PutPiece,
		StreamInfo,
	};

	WorkType type;
	std::string stream;
	uint64_t piece_num; // only PutPiece
	std::vector<char> data; // only PutPiece
	uint16_t last_len; // only EndStream
	std::function<void()> done; // only EndStream
	double unix_time; // only StreamInfo
	std::string callid; // only StreamInfo

	static PutWork end(std::string s, uint16_t last, std::function<void()> f) {
		PutWork ret{};
		ret.type = EndStream;
		ret.stream = std::move(s);
		ret.last_len = last;
		ret.done = f;
		return ret;
	}

	static PutWork put(std::string s, uint64_t p, std::vector<char> d) {
		PutWork ret{};
		ret.type = PutPiece;
		ret.stream = std::move(s);
		ret.piece_num = p;
		ret.data = std::move(d);
		return ret;
	}

	static PutWork set(std::string s, double unix_time, std::string callid) {
		PutWork ret{};
		ret.type = StreamInfo;
		ret.stream = std::move(s);
		ret.unix_time = unix_time;
		ret.callid = std::move(callid);
		return ret;
	}
};

struct GetWork {
	std::string streamid;
	uint64_t piece_num;
	std::function<void(bool, std::vector<char> d)> get_func;
};

static const char* stmt_stream_insert = "stmt_stream_insert";
static const char* stmt_piece_insert = "stmt_piece_insert";
static const char* stmt_end_streams = "stmt_end_streams";
static const char* stmt_get_streams = "stmt_get_streams";
static const char* stmt_set_streaminfo = "stmt_set_streaminfo";

static std::shared_ptr<pqxx::lazyconnection> create_connection(const std::string& uri) {
	auto ret = std::make_shared<pqxx::lazyconnection>(uri);

	ret->prepare(stmt_stream_insert,
R"(INSERT INTO streams(streamid, current)
VALUES(json_array_elements_text($1::json),TRUE)
ON CONFLICT DO NOTHING)");

	ret->prepare(stmt_piece_insert,
R"(INSERT INTO pieces(streamid, piecenum, data)
VALUES(
	json_array_elements_text($1::json),
	json_array_elements_text($2::json)::bigint,
	json_array_elements_text($3::json)::bytea
) ON CONFLICT(streamid, piecenum)
DO UPDATE SET
	data = EXCLUDED.data
WHERE
	pieces.streamid = EXCLUDED.streamid
	AND pieces.piecenum = EXCLUDED.piecenum)");

	ret->prepare(stmt_end_streams,
R"(UPDATE streams
SET
	current = FALSE,
	last_len = n.l
FROM (
	SELECT *
	FROM json_to_recordset($1)
		AS n(s text, l smallint)
) n
WHERE streamid = n.s)");

	ret->prepare(stmt_get_streams,
R"(SELECT n.s, n.p, data
FROM pieces RIGHT JOIN json_to_recordset($1) AS n(s text, p bigint)
ON pieces.streamid = n.s AND pieces.piecenum = n.p
)");

	ret->prepare(stmt_set_streaminfo,
R"(INSERT INTO streams(streamid, unix_time, callid)
SELECT streamid, to_timestamp(unix_time) AS unix_time, callid FROM json_to_recordset($1) AS n(streamid TEXT, unix_time double precision, callid TEXT)
ON CONFLICT(streamid) DO UPDATE SET
	unix_time = EXCLUDED.unix_time,
	callid = EXCLUDED.callid
WHERE streams.streamid = EXCLUDED.streamid
)");

	return ret;
}

struct irr_data::impl {
	boost::asio::io_service& io_service;

	// is only used in the thread but created outside of it
	// this can be set to null in the main thread (config_notify)
	// and be set to not-null in the database thread
	// this is protected by mutex
	std::shared_ptr<pqxx::lazyconnection> database_connection_int;
	std::shared_ptr<pqxx::lazyconnection> get_connection() {
		std::lock_guard<std::mutex> guard{mutex};
		if (database_connection_int)
			return database_connection_int;
		else {
			auto config = config_get();
			database_connection_int = create_connection(config->db_uri());
			return database_connection_int;
		}
	}

	std::shared_ptr<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>> on_change_func;

	std::thread db_thread;

	// mutex and condition variable are for both works and put_queue
	std::mutex mutex;
	std::condition_variable cv;
	// arbitrary work items, done one at a time in-order
	std::queue<std::function<void()>> works;
	// PUT queue (put_piece & end_stream), must execute as-if in-order for each stream
	std::queue<PutWork> put_queue;
	// GET queue
	std::queue<GetWork> get_queue;

	impl(boost::asio::io_service& io): io_service{io} { }

	// add to the work queue
	void add(std::function<void()> f) {
		{
			std::lock_guard<std::mutex> guard{mutex};
			works.push(f);
		}
		cv.notify_one();
	}

	// add to the put queue
	void add_put(PutWork w) {
		{
			std::lock_guard<std::mutex> guard{mutex};
			put_queue.emplace(std::move(w));
		}
		cv.notify_one();
	}

	void add_get(GetWork w) {
		{
			std::lock_guard<std::mutex> guard{mutex};
			get_queue.emplace(std::move(w));
		}
		cv.notify_one();
	}
};

#include <iostream>

irr_data::irr_data(boost::asio::io_service& io_service)
: impl_{ new impl{io_service}, [](impl* i){delete i;} } {
	impl_->on_change_func = std::make_shared<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>>(
	[this] (std::shared_ptr<const irr_config> before, std::shared_ptr<const irr_config> after) {
		if (before->db_uri() != after->db_uri()) {
			std::lock_guard<std::mutex> guard{impl_->mutex};
			impl_->database_connection_int = nullptr;
		}
	});
	config_subscribe(impl_->on_change_func);

	impl_->db_thread = std::thread{[&] () {
		Json::FastWriter writer;

		while (true) {
			std::function<void()> f = [] () {};

			// PUT queue variables
			Json::Value insert_streams{Json::arrayValue};
			Json::Value insert_pieces{Json::arrayValue};
			std::vector<pqxx::binarystring> insert_data;
			std::set<std::string> insert_stream_set;
			// streamid -> (unix_time, callid)
			std::map<std::string,std::tuple<double,std::string>> set_info;

			std::vector<std::string> end_streams;
			std::vector<uint16_t> end_last_len;
			std::vector<std::function<void()>> end_done;

			// GET queue variables
			std::map<std::pair<std::string, uint64_t>, std::vector<std::function<void(bool, std::vector<char> d)>>> get_pieces;
			{
				std::unique_lock<std::mutex> guard{impl_->mutex};
				while (impl_->works.empty() && impl_->put_queue.empty() && impl_->get_queue.empty())
					impl_->cv.wait(guard);
				if (!impl_->works.empty()) {
					f = impl_->works.front();
					impl_->works.pop();
				}

				for (size_t i = 0; i < 200; ++i) {
					if (impl_->put_queue.empty())
						break;
					PutWork p = impl_->put_queue.front();
					impl_->put_queue.pop();
					insert_stream_set.insert(p.stream);
					switch (p.type) {
						case PutWork::PutPiece:
							insert_streams.append(p.stream);
							insert_pieces.append(pqxx::to_string(p.piece_num));
							insert_data.push_back(pqxx::binarystring{p.data.data(), p.data.size()});
							break;
						case PutWork::EndStream:
							end_streams.push_back(p.stream);
							end_last_len.push_back(p.last_len);
							end_done.push_back(p.done);
							break;
						case PutWork::StreamInfo:
							set_info[p.stream] = std::make_tuple(p.unix_time, p.callid);
							break;
					}
				}

				for (size_t i = 0; i < 200; ++i) {
					if (impl_->get_queue.empty())
						break;
					GetWork g = impl_->get_queue.front();
					impl_->get_queue.pop();
					get_pieces[std::make_pair(g.streamid, g.piece_num)].push_back(g.get_func);
				}
			}
			f();

			Json::Value set_streams_vec{Json::arrayValue};
			for (auto&& e: set_info) {
				Json::Value v;
				v["streamid"] = e.first;
				v["unix_time"] = std::get<0>(e.second);
				v["callid"] = std::get<1>(e.second);
				set_streams_vec.append(v);
			}

			Json::Value insert_stream_set_vec{Json::arrayValue};
			for(auto&& s: insert_stream_set)
				insert_stream_set_vec.append(s);

			// PUT transaction
			transactor([&] () {
				std::shared_ptr<pqxx::lazyconnection> c = impl_->get_connection();
				pqxx::work w{*c};

				Json::Value insert_data_esc{Json::arrayValue};
				for (auto&& e: insert_data)
					insert_data_esc.append(w.esc_raw(e.data(), e.size()));

				w.prepared(stmt_set_streaminfo)
					(writer.write(set_streams_vec)).exec();
				w.prepared(stmt_stream_insert)
					(writer.write(insert_stream_set_vec)).exec();
				w.prepared(stmt_piece_insert)
					(writer.write(insert_streams))
					(writer.write(insert_pieces))
					(writer.write(insert_data_esc)).exec();

				Json::Value finish_array{Json::arrayValue};
				for (size_t i = 0; i < end_streams.size(); ++i) {
					Json::Value elem;
					elem["s"] = end_streams[i];
					elem["l"] = end_last_len[i];
					finish_array.append(elem);
				}
				w.prepared(stmt_end_streams)(writer.write(finish_array)).exec();
				w.commit();

				return 0;
			}, "PUT transaction");

			for (auto&& done: end_done)
				impl_->io_service.post(done);

			auto pieces = transactor([&] () {
				std::map<std::pair<std::string, uint64_t>, std::pair<bool, std::vector<char>>> ret;
				Json::Value get_array{Json::arrayValue};
				for (auto&& e: get_pieces) {
					Json::Value obj;
					obj["s"] = e.first.first;
					obj["p"] = static_cast<Json::Int64>(e.first.second);
					get_array.append(obj);
				}
				std::shared_ptr<pqxx::lazyconnection> c = impl_->get_connection();
				pqxx::read_transaction w{*c};

				auto got = w.prepared(stmt_get_streams)
					(writer.write(get_array)).exec();

				for (auto&& row: got) {
					std::vector<char> vec;
					bool valid;
					if (row.at(2).is_null()) {
						valid = false;
					} else {
						pqxx::binarystring bs{row.at(2)};
						vec.assign(bs.begin(), bs.end());
						valid = true;
					}
					ret[std::make_pair(row.at(0).as<std::string>(), row.at(1).as<uint64_t>())] = std::make_pair(valid, vec);
				}

				w.commit();
				return ret;
			}, "GET transaction");

			// we do all the posts within the io_service so that all the gets happen in
			// rapid succession and fill up the send buffers so that we can send in as
			// big of packets as possible
			auto& tmp_io_service = impl_->io_service;
			impl_->io_service.post([pieces, get_pieces, &tmp_io_service] () mutable {
				for (auto&& piece: pieces) {
					bool valid = piece.second.first;
					std::vector<char> vec = piece.second.second;
					for (auto&& func: get_pieces[piece.first]) {
						tmp_io_service.post([func, valid, vec] () { func(valid, vec); });
					}
				}
			});
		}
	}};
}

void irr_data::put_piece(std::string s, uint64_t p, std::vector<char> d) {
	impl_->add_put(PutWork::put(s, p, std::move(d)));
}

void irr_data::get_piece(std::string streamid, uint64_t p, std::function<void(bool, std::vector<char> d)> f) {
	GetWork w;
	w.streamid = streamid;
	w.piece_num = p;
	w.get_func = f;
	impl_->add_get(w);
}

void irr_data::end_stream(std::string streamid, uint16_t last_len, std::function<void()> f) {
	impl_->add_put(PutWork::end(streamid, last_len, std::move(f)));
}

void irr_data::set_streaminfo(std::string streamid, double unix_time, std::string callid) {
	impl_->add_put(PutWork::set(std::move(streamid), unix_time, std::move(callid)));
}

void irr_data::list_streams(std::function<void(std::vector<std::string>)> f) {
	impl_->add([=] () {
		std::vector<std::string> streams = transactor([&] () {
			std::vector<std::string> ret;
			std::shared_ptr<pqxx::lazyconnection> c = impl_->get_connection();
			pqxx::read_transaction w{*c};
			pqxx::result res = w.exec("SELECT streamid FROM streams");
			for(auto&& row: res) {
				ret.push_back(row.at(0).as<std::string>());
			}
			w.commit();
			return ret;
		}, "list_streams transaction");
		impl_->io_service.post([streams, f] () {f(streams);});
	});
}

void irr_data::list_current_streams(std::function<void(std::vector<std::string>)> f) {
	impl_->add([=] () {
		std::vector<std::string> current_streams = transactor([&] () {
			std::shared_ptr<pqxx::lazyconnection> c = impl_->get_connection();
			pqxx::read_transaction w{*c};
			pqxx::result res = w.exec("SELECT streamid FROM streams WHERE current = TRUE");
			std::vector<std::string> ret;
			for(auto&& row: res) {
				ret.push_back(row.at(0).as<std::string>());
			}
			w.commit();
			return ret;
		}, "list_current_streams transaction");
		impl_->io_service.post([current_streams, f] () {f(current_streams);});
	});
}

void irr_data::get_streaminfo(std::string s, std::function<void(bool,stream_info)> f) {
	impl_->add([=] () {
		stream_info info;
		bool valid;

		std::tie(valid, info) = transactor([&] () {
			stream_info ret{};
			bool ret_valid;
			std::shared_ptr<pqxx::lazyconnection> c = impl_->get_connection();
			pqxx::read_transaction w{*c};
			pqxx::result res1 = w.parameterized("SELECT last_len, current, EXTRACT(epoch FROM unix_time), callid FROM streams WHERE streamid = $1")(s).exec();
			if (res1.size() == 0) {
				ret_valid = false;
				ret = stream_info{};
			} else {
				ret_valid = true;
				ret.last_len = res1.at(0).at(0).as<uint16_t>();
				ret.current = res1.at(0).at(1).as<bool>();
				ret.unix_time = res1.at(0).at(2).as<double>();
				ret.callid = res1.at(0).at(3).as<std::string>();
				pqxx::result res2 = w.parameterized("SELECT piecenum from pieces WHERE streamid = $1 ORDER BY 1 DESC LIMIT 1")(s).exec();
				if (res2.size() == 0)
					ret.num_pieces = 0;
				else
					ret.num_pieces = res2.at(0).at(0).as<int64_t>() + 1;
			}
			w.commit();
			return std::make_pair(ret_valid, ret);
		}, "get_streaminfo transacation");
		impl_->io_service.post([f, info, valid] () {f(valid, info);});
	});
}

void irr_data::latest_n_retention(size_t n) {
	IRR_NOTICE("DB latest_n_retention: " << n);
	std::thread async{[n] () {
		transactor_once([=] () {
			// we do our own separate connection for this because it takes longer
			// than most other transactions, but doesn't conflict with them (normally)
			auto config = config_get();
			pqxx::connection c{config->db_uri()};
			pqxx::work w{c};

			w.exec("SET lock_timeout='5min'");
			w.exec("SELECT pg_advisory_xact_lock(50)");

			// this deletes from table pieces as well because of ON DELETE CASCADE.
			// this deletes all but current calls, and the last n non-current calls
			// ordered by the start time of the first stream in the call
			pqxx::result result = w.parameterized(R"(
DELETE FROM streams WHERE callid NOT IN (
		(SELECT callid FROM streams
		GROUP BY callid
		HAVING NOT bool_or(current)
		ORDER BY min(unix_time) DESC LIMIT $1)
	UNION
		(SELECT callid FROM streams WHERE current = TRUE)
))"
			)(n).exec();

			IRR_NOTICE("DB latest_n_retention deleted " << result.affected_rows() << " streams");

			w.commit();
		}, "latest_n_retention transaction");
	}};

	// we let this thread run on its own, we don't need to keep track of it
	async.detach();
}

void irr_data::seconds_retention(size_t seconds) {
	IRR_NOTICE("DB seconds_retention: " << seconds);
	std::thread async([seconds] () {
		// we do our own separate connection for this because it takes longer
		// than most other transactions, but doesn't conflict with them (normally)
		auto config = config_get();
		pqxx::connection c{config->db_uri()};
		pqxx::work w{c};

		w.exec("SET lock_timeout='5min'");
		w.exec("SELECT pg_advisory_xact_lock(50)");

		pqxx::result result = w.parameterized(R"(
DELETE FROM streams WHERE callid IN (
	SELECT callid
	FROM streams
	GROUP BY callid
	HAVING NOT bool_or(current)
		AND now() - max(unix_time
			+ '20ms'::interval * COALESCE(
				(SELECT piecenum + 1 FROM pieces WHERE streams.streamid = pieces.streamid ORDER BY piecenum DESC LIMIT 1),
				0
			)
		) > $1
))")(seconds).exec();

	pqxx::result result2 = w.parameterized(R"(
DELETE FROM streams WHERE callid IS NULL
	AND NOT current
	AND now() - (unix_time
		+ '20ms'::interval * COALESCE(
			(SELECT piecenum + 1 FROM pieces WHERE streams.streamid = pieces.streamid ORDER BY piecenum DESC LIMIT 1),
			0
		)
	) > $1
)")(seconds).exec();

		IRR_NOTICE("DB seconds_retention deleted " << (result.affected_rows() + result2.affected_rows()) << " streams");

		w.commit();
	});

	// we let this thread run on its own, we don't need to keep track of it
	async.detach();
}
