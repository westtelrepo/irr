#ifndef WESTTEL_IRR_DATA_r3mU3XOLeL
#define WESTTEL_IRR_DATA_r3mU3XOLeL

#include <boost/asio.hpp>
#include <functional>
#include <string>
#include <vector>

struct stream_info {
	bool current;
	uint16_t last_len;
	int64_t num_pieces;
	double unix_time; // time at start of stream
	std::string callid;
};

class irr_data {
private:
	struct impl;
	// PIMPL with a custom deleter because sizeof impl is only known in irr_data.cpp
	std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
	// io_service is the io_service to post callbacks to
	irr_data(boost::asio::io_service& io_service);

	// PUT queue
	void put_piece(std::string s, uint64_t p, std::vector<char> d);
	void end_stream(std::string s, uint16_t last_len, std::function<void()> f);
	void set_streaminfo(std::string s, double unix_time, std::string callid);

	// GET queue
	void get_piece(std::string s, uint64_t p, std::function<void(bool, std::vector<char> d)> f);

	void list_streams(std::function<void(std::vector<std::string>)> f);
	void list_current_streams(std::function<void(std::vector<std::string>)> f);

	// f(exists, stream_info)
	void get_streaminfo(std::string s, std::function<void(bool,stream_info)> f);

	// delete all but the latest n calls
	void latest_n_retention(size_t n);

	// delete all calls older than seconds
	void seconds_retention(size_t seconds);
};

#endif
