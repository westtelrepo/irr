#include "config.h"
#include "logger.h"

#include <iostream>

#define SD_JOURNAL_SUPPRESS_LOCATION
#include <systemd/sd-journal.h>

void irr_logger::log(int severity, std::string message, const char* file, int line) {
    auto config = config_get();

    if (!config) return;

    if (severity > config->log_level()) return;

    switch (config->log_dest()) {
        case log_dest::Stdout:
            std::cout << "<" << severity << ">" << message << std::endl;
            break;
        case log_dest::systemd:
            sd_journal_send(
                "MESSAGE=%s", message.c_str(),
                "PRIORITY=%i", severity,
                "CODE_FILE=%s", file,
                "CODE_LINE=%i", line,
                nullptr
            );
            break;
    }
}

irr_logger& global_irr_logger() {
    static irr_logger logger;
    return logger;
}
