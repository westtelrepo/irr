#ifndef IRR_LOGGER_ALERTS_HEADER
#define IRR_LOGGER_ALERTS_HEADER

#include <sstream>
#include <string>

class irr_logger {
private:
public:
    irr_logger() {}

    void log(int severity, std::string message, const char* filename, int linenum);
};

irr_logger& global_irr_logger();

#define IRR_LOG(severity, message) do {std::ostringstream _ss; _ss << message; global_irr_logger().log(severity, _ss.str(), __FILE__, __LINE__); } while(0)

#define IRR_CRIT(message) IRR_LOG(2, message)
#define IRR_ERR(message) IRR_LOG(3, message)
#define IRR_WARN(message) IRR_LOG(4, message)
#define IRR_NOTICE(message) IRR_LOG(5, message)
#define IRR_INFO(message) IRR_LOG(6, message)
#define IRR_DEBUG(message) IRR_LOG(7, message)

#endif
