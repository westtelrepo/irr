#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/system_timer.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <chrono>
#include <croncpp.h>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <json/json.h>
#include <mutex>
#include <set>
#include <string>
#include <sys/resource.h>
#include <systemd/sd-daemon.h>
#include <thread>

#include "../common/posixFd.h"
#include "../common/tcp_frame.h"
#include "config.h"
#include "globals.h"
#include "irr_data.h"
#include "logger.h"
#include "os_release.h"
#include "ptk_client.h"
#include "rtp.h"
#include "wav_reader.h"

static_assert(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__, "Little endian only");

static uint64_t SD_WATCHDOG_USEC = 0;

static void print_diag_info() {
	timespec monotonic_res{};
	int e1 = clock_getres(CLOCK_MONOTONIC, &monotonic_res);

	if (e1 != 0) {
		IRR_ERR("Could not get CLOCK_MONOTONIC resolution: " << strerror(errno));
	}

	timespec realtime_res{};
	int e2 = clock_getres(CLOCK_REALTIME, &realtime_res);

	if (e2 != 0) {
		IRR_ERR("Could not get CLOCK_REALTIME resolution: " << strerror(errno));
	}

	IRR_NOTICE("Diagnostic info:\n"
		<< "CLOCK_MONOTINIC resolution: " << (monotonic_res.tv_sec + monotonic_res.tv_nsec * 1e-9) << "s\n"
		<< "CLOCK_REALTIME resolution: " << (realtime_res.tv_sec + realtime_res.tv_nsec * 1e-9) << "s\n"
		<< (SD_WATCHDOG_USEC != 0
			? "systemd watchdog enabled with " + std::to_string(SD_WATCHDOG_USEC / 1000000.0) + " seconds"
			: "systemd watchdog not enabled"
		)
	);
}

static int run_tests(size_t samples) {
	int exit_code = EXIT_SUCCESS;

	timespec monotonic_res{};
	timespec realtime_res{};

	int e1 = clock_getres(CLOCK_MONOTONIC, &monotonic_res);
	if (e1 != 0) {
		std::cout << "FAILURE: Could not get resolution of CLOCK_MONOTONIC" << std::endl;
		exit_code = EXIT_FAILURE;
	} else {
		double res = monotonic_res.tv_sec + 1e-9 * monotonic_res.tv_nsec;
		if (res > 0.001) {
			std::cout << "FAILURE: CLOCK_MONOTONIC has resolution of " << res << "s" << std::endl;
			exit_code = EXIT_FAILURE;
		} else {
			std::cout << "CLOCK_MONOTONIC resolution: " << res << "s" << std::endl;
		}
	}

	int e2 = clock_getres(CLOCK_REALTIME, &realtime_res);
	if (e2 != 0) {
		std::cout << "FAILURE: Could not get resolution of CLOCK_REALTIME" << std::endl;
		exit_code = EXIT_FAILURE;
	} else {
		double res = realtime_res.tv_sec + 1e-9 * realtime_res.tv_nsec;
		if (res > 0.001) {
			std::cout << "FAILURE: CLOCK_REALTIME has resolution of " << res << "s" << std::endl;
			exit_code = EXIT_FAILURE;
		} else {
			std::cout << "CLOCK_REALTIME resolution: " << res << "s" << std::endl;
		}
	}

	std::cout << "Starting clock stability test (" << samples << " samples)..." << std::endl;

	// 20 ms
	timespec time_dur;
	time_dur.tv_sec = 0;
	time_dur.tv_nsec = 20000000;

	double max_interval = 0;
	double min_interval = std::numeric_limits<double>::infinity();
	double avg_interval = 0;

	for (size_t i = 0; i < samples; i++) {
		timespec before_spec{};
		if (clock_gettime(CLOCK_MONOTONIC, &before_spec) != 0) {
			std::cout << "FAILURE: could not get CLOCK_MONOTONIC: " << strerror(errno) << std::endl;
			exit_code = EXIT_FAILURE;
			break;
		}

		if (clock_nanosleep(CLOCK_MONOTONIC, 0, &time_dur, nullptr) != 0) {
			std::cout << "FAILURE: could not clock_nanosleep: " << strerror(errno) << std::endl;
			exit_code = EXIT_FAILURE;
			break;
		}

		timespec after_spec{};
		if (clock_gettime(CLOCK_MONOTONIC, &after_spec) != 0) {
			std::cout << "FAILURE: could not get CLOCK_MONOTONIC: " << strerror(errno) << std::endl;
			exit_code = EXIT_FAILURE;
			break;
		}

		double before = before_spec.tv_sec + 1e-9 * before_spec.tv_nsec;
		double after = after_spec.tv_sec + 1e-9 * after_spec.tv_nsec;

		double interval = after - before;
		if (interval >= 0) {
			avg_interval += interval;
			max_interval = std::max(max_interval, interval);
			min_interval = std::min(min_interval, interval);
		} else {
			std::cout << "FAILURE: got interval < 0" << std::endl;
			exit_code = EXIT_FAILURE;
			break;
		}
	}

	avg_interval /= samples;

	std::cout << "\tAvg interval: " << avg_interval * 1000 << "ms" << std::endl;
	std::cout << "\tMax interval: " << max_interval * 1000 << "ms" << std::endl;
	std::cout << "\tMin interval: " << min_interval * 1000 << "ms" << std::endl;

	if (max_interval > 0.1) {
		std::cout << "FAILURE: max interval > 100 ms" << std::endl;
		exit_code = EXIT_FAILURE;
	}

	if (avg_interval > 0.05) {
		std::cout << "FAILURE: avg interval > 50 ms" << std::endl;
		exit_code = EXIT_FAILURE;
	}

	if (exit_code) {
		std::cout << "FAILURE: some tests failed, see above" << std::endl;
	} else {
		std::cout << "All tests successful" << std::endl;
	}

	return exit_code;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static std::unique_ptr<irr_data> irrdata;
#pragma clang diagnostic pop

struct opusPiece {
	std::vector<char> data;
	uint64_t num;
	std::string stream;

	std::string string() {
		std::stringstream ss;
		ss << static_cast<char>(1);
		ss.write(reinterpret_cast<char*>(&num), sizeof num);
		ss << stream << static_cast<char>(0);
		ss.write(data.data(), static_cast<std::streamsize>(data.size()));
		return ss.str();
	}
};

class irr_session: public std::enable_shared_from_this<irr_session> {
private:
	tcp_frames socket_;
	std::queue<std::string> write_;

	std::string write_tmp_;

	// boost::circular_buffer<opusPiece> weak_buf_{100};
	// use a standard queue instead of a circular buffer for weak sending,
	// meaning that weak sending is not "weak".
	// currently the PSAP toolkit does not deal with dropouts, and they
	// are uncommon anyway.
	// keeping this note here in case so that in the future the IRR client
	// can signal an extension to say "I can handle this!" and use a much
	// smaller weak buffer (like 10 elements) to help deal with network
	// issues and the like.
	// keeping in two buffers makes it so large GET requests don't starve
	// out PLAY data when sent.
	std::queue<opusPiece> weak_buf_;
	std::queue<opusPiece> buf_;

	std::set<std::string> play_;

	boost::asio::io_service& io_service_;
public:
	irr_session(boost::asio::ip::tcp::socket&& s, boost::asio::io_service& io_service): socket_{std::move(s)}, io_service_{io_service} { }

	~irr_session() {
		IRR_INFO(socket_.connection_details() << ": connection closed");
	}

	void start() {
		IRR_DEBUG(socket_.connection_details() << ": connection started");
		read();
	}

	// send (weakly), but only if currently subscribed.
	void if_sub_send(std::string stream, uint64_t piece, std::vector<char> data) {
		if (play_.count(stream))
			send_weak(stream, piece, data);
	}

	void send(std::string stream, uint64_t piece, std::vector<char> data) {
		buf_.push(opusPiece{data, piece, stream});
		io_service_.post([self = shared_from_this(), this] () { write(); });
	}

	void send_weak(std::string stream, uint64_t piece, std::vector<char> data) {
		weak_buf_.push(opusPiece{data, piece, stream});
		io_service_.post([self = shared_from_this(), this] () { write(); });
	}

	void end_stream(std::string stream, uint16_t last_len) {
		// only send PLAYEND if currently playing
		if (play_.count(stream)) {
			Json::Value ret;
			ret["_c"] = "PLAYEND";
			ret["last_len"] = last_len;
			ret["stream"] = stream;
			send(ret);
			play_.erase(stream);
		}
	}

	void send(const Json::Value& v) {
		Json::FastWriter w;
		write_.push("\2" + w.write(v));
		write();
	}
private:
	void read() {
		socket_.async_read([this, self = shared_from_this()] (const boost::system::error_code& e, std::vector<char> data) {
			if (e || data.size() < 1)
				return;

			switch (data.at(0)) {
				case 1:
					return;
					// Opus piece... we should never get it here (immediately break connection)
				case 2: {
					// JSON object!
					std::stringstream ss{std::string{data.data() + 1, data.size() - 1}};
					Json::Value root;

					// try to parse it, if we can't, close the connection
					try {
						ss >> root;
					} catch (const std::exception& e) {
						IRR_WARN(socket_.connection_details() << ": Got invalid JSON, closing connection: " << e.what());
						return;
					}

					Json::FastWriter fast_writer;
					IRR_DEBUG(socket_.connection_details() << ": received " << fast_writer.write(root));

					int64_t r = 0;
					if (root.isMember("_r"))
						r = root["_r"].asInt64();

					if (r < 0) {
						// this is a response, and we don't do anything with responses currently
						// TODO: ping
					} else {
						if (!root.isMember("_c")) {
							IRR_WARN(socket_.connection_details() << ": Got frame without command, closing connection");
							return;
						}
						std::string command = root["_c"].asString();

						Json::Value ret;
						ret["_r"] = static_cast<Json::Int64>(-r);
						if (command == "PING" && r > 0) {
							ret["_c"] = "OK";
							send(ret);
						} else if (command == "LIST" && r > 0) {
							irrdata->list_streams(
							[this, self, ret] (std::vector<std::string> streams) mutable {
								Json::Value list{Json::arrayValue};
								for (auto&& p: streams) {
									list.append(p);
								}
								ret["streams"] = list;
								ret["_c"] = "OK";
								send(ret);
							});
						} else if (command == "LISTCURRENT" && r > 0) {
							irrdata->list_current_streams(
							[this, self, ret] (std::vector<std::string> streams) mutable {
								Json::Value list{Json::arrayValue};
								for (auto&& p: streams) {
									list.append(p);
								}
								ret["current"] = list;
								ret["_c"] = "OK";
								send(ret);
							});
						} else if (command == "STREAMINFO" && r > 0) {
							irrdata->get_streaminfo(
							root["stream"].asString(),
							[this, self, ret] (bool valid, stream_info si) mutable {
								if (valid) {
									ret["_c"] = "OK";
									ret["num_pieces"] = static_cast<Json::Int64>(si.num_pieces);
									ret["current"] = si.current;
									ret["unix_time"] = si.unix_time;
									ret["callid"] = si.callid;
									if (!si.current && si.num_pieces > 0)
										ret["last_len"] = si.last_len;
								} else {
									ret["_c"] = "ERROR";
									ret["e"] = "EXIST";
								}
								send(ret);
							});
						} else if (command == "GET") {
							std::string streamid = root["stream"].asString();
							uint64_t low = root["first"].asUInt64();
							uint64_t high = root["last"].asUInt64();
							for (uint64_t i = low; i <= high; ++i) {
								irrdata->get_piece(streamid, i,
								[streamid, i, this, self] (bool valid, std::vector<char> pdata) {
									if (valid)
										send(streamid, i, std::move(pdata));
									else {
										Json::Value noget;
										noget["_c"] = "NOGET";
										noget["piece_num"] = static_cast<Json::UInt64>(i);
										noget["stream"] = streamid;
										send(noget);
									}
								});
							}
							if (r > 0) {
								ret["_c"] = "OK";
								send(ret);
							}
						} else if (command == "PLAY") {
							play_.insert(root["stream"].asString());
							if (r > 0) {
								ret["_c"] = "OK";
								send(ret);
							}
						} else if (command == "STOP") {
							play_.erase(root["stream"].asString());
							if (r > 0) {
								ret["_c"] = "OK";
								send(ret);
							}
						} else if (command == "LISTPLAY" && r > 0) {
							Json::Value list{Json::arrayValue};
							for (const auto& s: play_)
								list.append(s);
							ret["list"] = list;
							ret["_c"] = "OK";
							send(ret);
						} else if (command == "?" && r > 0) {
							ret["_c"] = "OK";
							ret["has"] = false;
							send(ret);
						} else if (command == "ENABLE") {
							if (r > 0) {
								ret["_c"] = "ERROR";
								ret["e"] = "EXIST";
								send(ret);
							}
							else {
								// if this extension doesn't exist to be enabled then the only way
								// of notifying is to disconnect
								IRR_WARN(socket_.connection_details()
									<< ": got unknown extension without possible response, closing connection");
								return;
							}
						} else {
							if (r > 0) {
								ret["_c"] = "UNKNOWN";
								send(ret);
							}
							else {
								// if this command is unknown and they did not ask for a response
								// then all we can do is disconnect
								IRR_WARN(socket_.connection_details() << ": Got unknown command, closing connection");
								return;
							}
						}
					}
					break;
				}
				default:
					IRR_WARN(socket_.connection_details() << ": Got unexpected frame type, closing connection");
					// we have no idea, close connection
					return;
			}

			read();
		});
	}

	void write() {
		if (write_tmp_ == "") {
			if (write_.size()) {
				write_tmp_ = std::move(write_.front());
				write_.pop();
				socket_.async_write(write_tmp_.data(), write_tmp_.size(),
				[this, self = shared_from_this()] (const boost::system::error_code& e) {
					if (e) return;
					write_tmp_ = "";
					write();
				});
			} else if (buf_.size() || weak_buf_.size()) {
				auto buffers = std::make_shared<std::vector<std::string>>();
				buffers->reserve(100);

				for (size_t i = 0; i < 50; ++i) {
					if (!buf_.size())
						continue;
					buffers->push_back(buf_.front().string());
					buf_.pop();
				}

				for (size_t i = 0; i < 50; ++i) {
					if (!weak_buf_.size())
						continue;
					buffers->push_back(weak_buf_.front().string());
					weak_buf_.pop();
				}

				write_tmp_ = "is writing";
				socket_.async_write_many(*buffers,
				[this, self = shared_from_this(), buffers] (const boost::system::error_code& e) {
					if (e) return;
					write_tmp_ = "";
					write();
				});
			}
		}
	}
};

class irr_server {
private:
	std::shared_ptr<boost::asio::ip::tcp::acceptor> acceptor_;
	std::vector<std::weak_ptr<irr_session>> children_;
	boost::asio::io_service& io_service_;
	std::shared_ptr<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>> config_notify_;
public:
	irr_server(boost::asio::io_service& io_service)
	: io_service_{io_service} {
		recreate_acceptor();

		config_notify_ = std::make_shared<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>>(
		[this] (std::shared_ptr<const irr_config> old, std::shared_ptr<const irr_config> new_config) {
			if (old->port() != new_config->port() || !acceptor_)
				recreate_acceptor();
		});

		config_subscribe(config_notify_);
	}

	void send(std::string stream, uint64_t piece, std::vector<char> data) {
		for (auto&& i: children_) {
			if (auto sess = i.lock()) {
				sess->if_sub_send(stream, piece, data);
			}
		}
	}

	void end_stream(std::string stream, uint16_t last_len) {
		for (auto&& i: children_) {
			if (auto sess = i.lock()) {
				sess->end_stream(stream, last_len);
			}
		}
	}
private:
	void accept() {
		if (!acceptor_) return;
		auto socket = std::make_shared<boost::asio::ip::tcp::socket>(io_service_);
		acceptor_->async_accept(*socket, [this, socket, acceptor = this->acceptor_](const boost::system::error_code& e) {
			if (!e) {
				// delete dead children
				for (auto i = children_.begin(); i != children_.end(); ) {
					if (i->expired())
						i = children_.erase(i);
					else
						++i;
				}

				socket->set_option(boost::asio::ip::tcp::no_delay{true});
				auto session = std::make_shared<irr_session>(std::move(*socket), io_service_);
				session->start();
				children_.push_back(session);
				accept();
			}
		});
	}

	void recreate_acceptor() {
		if (acceptor_) {
			acceptor_->close();
			acceptor_ = nullptr;
		}
		try {
			acceptor_ = std::make_shared<boost::asio::ip::tcp::acceptor>(io_service_, boost::asio::ip::tcp::endpoint{boost::asio::ip::tcp::v4(), config_get()->port()});
			global_irr_status->clear("port");
		} catch(const std::exception& e) {
			global_irr_status->set_alarm("port", alarm_priority::error, "Could not create listen port", e.what());
		}
		accept();
	}
};

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static std::string IRR_CONFIG_FILE = "/etc/opt/irr.yml";
#pragma clang diagnostic pop

static std::shared_ptr<irr_config> new_config() {
	std::ifstream config_file;
	config_file.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	config_file.open(IRR_CONFIG_FILE);
	std::stringstream ss;
	ss << config_file.rdbuf();

	return std::make_shared<irr_config>(ss.str());
}

class signal_handler {
private:
	boost::asio::signal_set signal_set_;
public:
	signal_handler(boost::asio::io_service& io_service)
	: signal_set_{io_service} {
		signal_set_.add(SIGUSR1); // gradual shutdown
		signal_set_.add(SIGUSR2); // print status
		signal_set_.add(SIGPIPE); // we ignore this, for esmtp's sake
		signal_set_.add(SIGHUP); // reload the config!
		wait();
	}
private:
	void wait() {
		signal_set_.async_wait([this](const boost::system::error_code& e, int sig_num) {
			if (!e) {
				if (sig_num == SIGUSR1 && global_graceful_shutdown == 0) {
					global_graceful_shutdown = 1;
					global_irr_status->notify();
					IRR_NOTICE("Received SIGUSR1, beginning graceful shutdown");
				} else if (sig_num == SIGUSR1 && global_graceful_shutdown == 1) {
					global_graceful_shutdown = 2;
					global_irr_status->notify();
					IRR_NOTICE("Received SIGUSR1 again, graceful shutdown will not wait for database");
				} else if (sig_num == SIGUSR2) {
					std::cout << global_irr_status->string_status() << std::endl;
				} else if (sig_num == SIGHUP) {
					IRR_NOTICE("reloading config file");
					sd_notify(0, "RELOADING=1");
					try {
						config_set(new_config());
						global_irr_status->clear("config");
						global_irr_status->notify_reload();
					} catch(const std::exception& e) {
						global_irr_status->set_alarm("config", alarm_priority::error, "Could not load IRR config file", e.what());
					} catch(...) {
						global_irr_status->set_alarm("config", alarm_priority::programming_error, "Could not load IRR config file", "unknown exception");
					}
					sd_notify(0, "READY=1");
					print_diag_info();
				}
			}
			wait();
		});
	}
};

static void retention_func(boost::asio::steady_timer& timer) {
	auto config = config_get();
	auto seconds = config->retention_age_seconds();
	if (seconds) {
		irrdata->seconds_retention(*seconds);
	}
	irrdata->latest_n_retention(config->retention_limit());
	timer.expires_from_now(config->retention_interval());
	timer.async_wait([&timer] (const boost::system::error_code& e) { if (e) return; retention_func(timer); });
}

static void notify_func(boost::asio::steady_timer& timer) {
	global_irr_status->notify();
	timer.expires_from_now(std::chrono::seconds(60));
	timer.async_wait([&timer] (boost::system::error_code) { notify_func(timer); });
}

class cron_holder: public std::enable_shared_from_this<cron_holder> {
private:
	boost::asio::system_timer timer;
	time_t last_time;
	cron::cronexpr expr;
	std::function<void()> func;
public:
	cron_holder(boost::asio::io_service& service, cron::cronexpr expr_, std::function<void()> func_)
	: timer{service}, last_time{time(nullptr)}, expr{expr_}, func{func_} { }

	void schedule() {
		last_time = cron::cron_next(expr, last_time);
		timer.expires_at(std::chrono::system_clock::from_time_t(last_time));
		auto self = this->shared_from_this();
		timer.async_wait([self] (const boost::system::error_code& e) {
			if (e) return;
			self->func();
			self->schedule();
		});
	}

	void cancel() {
		timer.cancel();
	}
};

// variables created by and populated by the Makefile and ./make_config_file.sh
extern const char* GIT_HASH;
extern const char* GIT_HASH_SHORT;
extern time_t BUILD_TIME;
extern const char* BUILD_LSB_DESC;
extern bool GIT_DIRTY;

// this follows the semantic versioning spec <https://semver.org/>
static std::string version() {
	std::array<char, 20> date_str;
	if (!strftime(date_str.data(), date_str.size(), "%Y%m%d", gmtime(&BUILD_TIME)))
		assert(false);
	return std::string{"1.2.1+"} + GIT_HASH_SHORT + (GIT_DIRTY ? std::string{"-dirty."} + date_str.data() : "");
}

static void watchdog_func(boost::asio::steady_timer& timer) {
	sd_notify(0, "WATCHDOG=1");
	auto now = std::chrono::time_point_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()).time_since_epoch().count();
	int64_t half_watchdog = static_cast<int64_t>(SD_WATCHDOG_USEC) / 2;

	auto new_time = half_watchdog * (now / half_watchdog) + half_watchdog;

	timer.expires_at(std::chrono::steady_clock::time_point{} + std::chrono::microseconds(new_time));
	timer.async_wait([&timer] (boost::system::error_code) { watchdog_func(timer); });
}

// NOTE: _never_ use the syscall `select`, never use anything based on `select`, don't even _think_ about it
// NOTE: `select` is pretty rare for new stuff these days
static void set_soft_limit_to_hard_limit() {
	struct rlimit rlim;
	if (getrlimit(RLIMIT_NOFILE, &rlim) == 0) {
		rlim.rlim_cur = rlim.rlim_max;
		if (setrlimit(RLIMIT_NOFILE, &rlim) == 0) {
			IRR_INFO("Successfully set soft RLIMIT_NOFILE to max of " << rlim.rlim_max);
		} else {
			IRR_WARN("Could not set RLIMIT_NOFILE: " << strerror(errno));
		}
	} else {
		IRR_WARN("Could not get current RLIMIT_NOFILE: " << strerror(errno));
	};
}

int main(int argc, char** argv) {
	// set this before argument setting so --test-config works
	if (const char* str = std::getenv("IRR_CONFIG_FILE"))
		IRR_CONFIG_FILE = std::string{str};

	size_t clock_test_samples = 50;

	std::string cron_argument;

	boost::program_options::options_description desc{"Options"};
	desc.add_options()
		("version,v", "Print version")
		("semver", "Print semantic version string")
		("test", "Run internal tests")
		("help,h", "Print out help message")
		("server", "Run IRR server")
		("test-config,t", "Test config file for errors")
		("config-file", boost::program_options::value<std::string>(&IRR_CONFIG_FILE), "Location of config file")
		("test-clock-samples", boost::program_options::value<size_t>(&clock_test_samples), "Number of timepoints to take for clock stability test")
		("test-cron", boost::program_options::value<std::string>(&cron_argument), "test a cron-style argument");

	boost::program_options::variables_map vm;
	boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);
	boost::program_options::notify(vm);

	if (vm.count("help")) {
		std::cout << desc << std::endl;
		return 0;
	} else if (vm.count("version")) {
		std::cout << "IRR version " << version() << std::endl;
		std::cout << std::endl;
		std::cout << "Build data:" << std::endl;
		std::cout << "LSB Description: " << BUILD_LSB_DESC << std::endl;

		std::cout << "Git hash: " << GIT_HASH;
		if (GIT_DIRTY)
			std::cout << " (dirty)";
		std::cout << std::endl;

		std::array<char, 100> date_str;
		if (strftime(date_str.data(), date_str.size(), "%c %Z(%z)", localtime(&BUILD_TIME))) {
			if (GIT_DIRTY)
				std::cout << "Build date: " << date_str.data() << std::endl;
			else
				std::cout << "Commit date: " << date_str.data() << std::endl;
		}

		const auto os_release = get_os_release();

		if (os_release.count("PRETTY_NAME")) {
			std::cout << std::endl;
			std::cout << "Run environment: " << get_os_release()["PRETTY_NAME"] << std::endl;
		}

		// TODO: include version data of dependencies
		return 0;
	} else if (vm.count("semver")) {
		std::cout << version() << std::endl;
		return 0;
	} else if (vm.count("test")) {
		return run_tests(clock_test_samples);
	} else if (vm.count("test-config")) {
		try {
			new_config();
		} catch(const std::exception& e) {
			std::cerr << "Could not parse config file(" << IRR_CONFIG_FILE << "): " << e.what() << std::endl;
			return 1;
		}
		std::cout << IRR_CONFIG_FILE << " parsed correctly" << std::endl;
		return 0;
	} else if (vm.count("test-cron")) {
		std::cout << "Testing '" << cron_argument << "'" << std::endl;
		cron::cronexpr cron = cron::make_cron(cron_argument);
		std::time_t now = std::time(0);
		for (int i = 0; i < 50; i++) {
			now = cron::cron_next(cron, now);
			std::cout << "time " << std::asctime(std::localtime(&now));
		}
		std::cout << "test successful" << std::endl;
		return 0;
	} else if (vm.count("server")) {
		// do nothing, fall through
	} else {
		std::cout << "irr requires argument, try --help for help" << std::endl;
		return 1;
	}

	config_set(new_config());
	IRR_WARN("starting up IRR server version " << version());

	// global_irr_status is never set again after this
	// so it is safe to use it from multiple threads
	global_irr_status = std::make_shared<irr_status>();
	global_irr_status->notify();

	boost::asio::io_service io_service;

	boost::asio::steady_timer sd_watchdog_timer{io_service};
	if (sd_watchdog_enabled(0, &SD_WATCHDOG_USEC) > 0) {
		sd_watchdog_timer.expires_from_now(std::chrono::microseconds(1));
		sd_watchdog_timer.async_wait([&sd_watchdog_timer] (const boost::system::error_code&) { watchdog_func(sd_watchdog_timer); });
	}

	set_soft_limit_to_hard_limit();
	print_diag_info();

	signal_handler handler{io_service};

	auto rtp_o = std::make_shared<rtp>(io_service);
	rtp_o->start();

	boost::asio::steady_timer retention_timer{io_service};
	retention_timer.expires_from_now(std::chrono::seconds(10));
	retention_timer.async_wait(
		[&retention_timer]
		(const boost::system::error_code& e) {
			if (e) return;
			retention_func(retention_timer);
		}
	);

	auto psap_client = ptk_client::create(io_service);
	psap_client->set_notify([&] (std::string streamid, double unix_time, std::set<int> positions) {
		rtp_o->notify(streamid, unix_time, positions);
	});
	psap_client->start();

	auto update_retention_timer = std::make_shared<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>>(
	[&retention_timer] (std::shared_ptr<const irr_config> old, std::shared_ptr<const irr_config> new_config) {
		// if the retention interval was changed, run the retention sooner
		// so that setting it to a smaller number works in a finite amount of time
		if (old->retention_interval() != new_config->retention_interval()) {
			retention_timer.expires_from_now(std::chrono::seconds(10));
			retention_timer.async_wait(
				[&retention_timer]
				(const boost::system::error_code& e) {
					if (e) return;
					retention_func(retention_timer);
				}
			);
		}
	});
	config_subscribe(update_retention_timer);

	// call notify on the global status occasionally to allow it to do stuff
	boost::asio::steady_timer status_notify_timer{io_service};
	status_notify_timer.expires_from_now(std::chrono::seconds(60));
	status_notify_timer.async_wait(
		[&status_notify_timer] (boost::system::error_code) {
			notify_func(status_notify_timer);
		}
	);

	// CRON STUFF
	std::vector<std::shared_ptr<cron_holder>> crons;

	auto update_crons = std::make_shared<std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)>>
	([&crons, &io_service] (std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config> new_config) {
		for (auto& e: crons) {
			e->cancel();
		}

		crons.clear();
		auto cron = new_config->cron();
		for (auto& c: cron) {
			auto e = std::make_shared<cron_holder>(io_service, c.expr, [c] () {
				IRR_DEBUG("Executing cron '" << cron::to_cronstr(c.expr) << "' with age " << c.age << " seconds");
				if (irrdata) {
					irrdata->seconds_retention(c.age);
				}
			});
			crons.push_back(e);
			e->schedule();
		}
	});
	(*update_crons)(config_get(), config_get());
	config_subscribe(update_crons);
	// AFTER CRON STUFF

	irrdata = std::make_unique<irr_data>(io_service);
	irr_server server{io_service};

	auto w = wavReader::create(io_service,
	[&] (std::string s, uint64_t p, std::vector<char> d) {
		server.send(s, p, d);
		rtp_o->send(s, p, d);
		irrdata->put_piece(s, p, std::move(d));
	},
	[&] (std::string s, uint16_t last_len, std::function<void()> f) {
		server.end_stream(s, last_len);
		rtp_o->stop(s);
		irrdata->end_stream(s, last_len, f);
	},
	[&] (std::string streamid, double unix_time, std::string callid) {
		irrdata->set_streaminfo(streamid, unix_time, callid);
		rtp_o->start(streamid, unix_time);
	});
	w->start();

	global_irr_status->notify_started();
	io_service.run();
}
