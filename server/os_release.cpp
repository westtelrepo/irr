#include "os_release.h"

#include <fstream>
#include <regex>
#include <sstream>

// the format for the /etc/os-release file is described https://www.freedesktop.org/software/systemd/man/os-release.html
// aka `man os-release`
// TODO: currently does not process escapes (\$, \`, \', \", etc) though I have not found
//   examples where they are used

std::map<std::string, std::string> parse_os_release(const std::string& file_contents) {
    #pragma clang diagnostic push
    #pragma clang diagnostic ignored "-Wexit-time-destructors"
    static const std::regex name_value{R"(\s*([_a-zA-Z0-9]+)\s*=\s*([_a-zA-Z0-9]+)\s*)"};
    static const std::regex name_value_single{R"(\s*([_a-zA-Z0-9]+)\s*=\s*\'([^'"$\\]*)\'\s*)"};
    static const std::regex name_value_double{R"(\s*([_a-zA-Z0-9]+)\s*=\s*\"([^'"$\\]*)\"\s*)"};
    #pragma clang diagnostic pop

    std::map<std::string, std::string> ret;
    std::istringstream in{file_contents};

    std::string line;
    while(std::getline(in, line)) {
        std::smatch m;
        if (std::regex_match(line, m, name_value)) {
            ret[m[1].str()] = m[2].str();
        } else if (std::regex_match(line, m, name_value_single)) {
            ret[m[1].str()] = m[2].str();
        } else if (std::regex_match(line, m, name_value_double)) {
            ret[m[1].str()] = m[2].str();
        }
    }

    return ret;
}

std::map<std::string, std::string> get_os_release() {
    // this does not throw an exception if the file does not exist
    // rather, it returns empty
    std::ifstream f{"/etc/os-release"};
    std::stringstream buffer;
    buffer << f.rdbuf();

    return parse_os_release(buffer.str());
}
