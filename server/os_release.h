#ifndef WESTTEL_OS_RELEASE
#define WESTTEL_OS_RELEASE

#include <map>
#include <string>

std::map<std::string, std::string> parse_os_release(const std::string& file_contents);

std::map<std::string, std::string> get_os_release();

#endif
