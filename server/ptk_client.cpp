#include "config.h"
#include "globals.h"
#include "logger.h"
#include "os_release.h"
#include "ptk_client.h"

#include <boost/asio/steady_timer.hpp>
#include <queue>
#include <regex>
#include <tinyxml2.h>

struct ptk_client::impl {
    boost::asio::io_service& io_service;

    std::shared_ptr<boost::asio::ip::tcp::socket> socket;

    boost::asio::steady_timer heartbeatTimer;
    boost::asio::steady_timer connectTimer;

    boost::asio::streambuf inbuf;
    std::queue<std::string> to_send;

    uint64_t hb = 1;

    int position;

    bool connecting = false;

    impl(boost::asio::io_service& io)
    : io_service{io}, heartbeatTimer{io}, connectTimer{io} {
        auto os_release = get_os_release();
        if (os_release.count("PRETTY_NAME")) {
            this->os_pretty_name = os_release["PRETTY_NAME"];
        } else {
            this->os_pretty_name = "OS Unknown";
        }
    }

    std::function<void(std::string,double,std::set<int>)> notify_func;
    std::function<void(std::shared_ptr<const irr_config>,std::shared_ptr<const irr_config>)> config_update;

    // PRETTY_NAME from os-release
    std::string os_pretty_name;
};

ptk_client::ptk_client(boost::asio::io_service& io_service)
: impl_{ new impl{io_service}, [](impl* i){ delete i; } } {
}

void ptk_client::send(std::string s) {
    bool was_empty = impl_->to_send.empty();
    impl_->to_send.emplace(std::move(s));
    if (was_empty)
        send_cycle();
}

void ptk_client::send_cycle() {
    if (!impl_->to_send.size())
        return;

    auto socket = impl_->socket;
    if (!socket) {
        while (impl_->to_send.size())
            impl_->to_send.pop();
        return;
    }

    std::string& s = impl_->to_send.front();
    auto buf = std::make_shared<std::string>("\nContent-Length: " + std::to_string(s.size() + 1) + "\nContent-Type: text/xml\n\n" + s + "\n");

    boost::asio::async_write(*socket, boost::asio::buffer(buf->data(), buf->size()), [self = shared_from_this(), this, buf, socket] (const boost::system::error_code& e, size_t) {
        impl_->to_send.pop();
        if (e) {
            IRR_ERR("PSAP toolkit write error: " << e);
            impl_->socket.reset();
            connect_cycle();
            return;
        }
        send_cycle();
    });
}

void ptk_client::heartbeat() {
    tinyxml2::XMLPrinter xml;
    xml.PushHeader(false, true);
    xml.OpenElement("Event");
    xml.PushAttribute("id", ("HB" + std::to_string(impl_->hb++)).c_str());
    xml.PushAttribute("XMLspec", "6.76");
    xml.PushAttribute("WindowsUser", "Instant Recall Recorder");
    xml.OpenElement("Heartbeat");
    xml.PushAttribute("ListenPort", "8080"); // todo: fix
    xml.PushAttribute("Position", std::to_string(impl_->position).c_str());
    xml.PushAttribute("GUIversion", "2.0.2.52"); // todo: fix
    xml.PushAttribute("OS", impl_->os_pretty_name.c_str());
    xml.CloseElement();
    xml.CloseElement();

    send(xml.CStr());

    impl_->heartbeatTimer.expires_from_now(std::chrono::seconds(6));
    impl_->heartbeatTimer.async_wait(std::bind(&ptk_client::heartbeat, this->shared_from_this()));
}

void ptk_client::start() {
    impl_->config_update = [this] (std::shared_ptr<const irr_config> old, std::shared_ptr<const irr_config> new_config) {
        if (old->ptk_address() != new_config->ptk_address() || old->ptk_port() != new_config->ptk_port() || old->ptk_position() != new_config->ptk_position()) {
            if (impl_->socket)
                impl_->socket->close();
            IRR_WARN("Got new controller connection parameters, resetting connection");
            connect_cycle();
        }
    };

    config_subscribe(
        std::shared_ptr<
            std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)>
        >{shared_from_this(), &impl_->config_update}
    );
    heartbeat();
    connect_cycle();
}

void ptk_client::read_cycle() {
    auto socket = impl_->socket;
    if (!socket)
        return;

    boost::asio::async_read_until(*socket, impl_->inbuf, "\n\n",
    [self = shared_from_this(), this, socket] (const boost::system::error_code& e, size_t datalen) {
        try {
            if (e) {
                IRR_ERR("PSAP toolkit read error: " << e);
                impl_->socket.reset();
                connect_cycle();
                return;
            }

            std::vector<char> data;
            data.resize(datalen);

            std::istream is{&impl_->inbuf};
            // throw exceptions, though this should never happen
            is.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);

            bool type = false;
            size_t size = 0;
            size_t find_two = 0;

            // read until we find two newlines in a row
            while (find_two < 2) {
                std::string str;
                std::getline(is, str);

                if (str.find("Content-Type:") != str.npos) {
                    type = true;
                } else if (str.find("Content-Length:") != str.npos) {
                    size = std::stoul(str.substr(str.find("Content-Length:") + 15));
                } else if(str == "") {
                    find_two++;
                } else {
                    find_two = 0;
                }
            }

            size_t to_read = size - std::min(size, impl_->inbuf.size());

            boost::asio::async_read(*socket, impl_->inbuf, boost::asio::transfer_at_least(to_read),
            [self = shared_from_this(), this, size, socket] (const boost::system::error_code& e2, size_t) {
                if (e2) {
                    IRR_ERR("PSAP toolkit read error: " << e2);
                    impl_->socket.reset();
                    connect_cycle();
                    return;
                }

                std::vector<char> data2;
                data2.resize(size);

                std::istream is2{&impl_->inbuf};
                is2.read(data2.data(), static_cast<std::streamsize>(data2.size()));

                process(data2.data(), data2.size());

                read_cycle();
            });
        } catch (const std::exception& e) {
            IRR_ERR("Error in PTK read: " << e.what());
            impl_->socket.reset();
            connect_cycle();
            return;
        }
    });
}

std::set<int> parse_position_string(std::string s, char type) {
    std::set<int> ret;

    for (size_t i = 0; i < s.size(); i++) {
        if (isspace(s[i])) {
            // eat
        } else if (s[i] == type) {
            std::string number;
            i++;
            for (; i < s.size() && isdigit(s[i]); i++) {
                number += s[i];
            }

            ret.insert(std::stoi(number));
        } else {
            for (; i < s.size() && !isspace(s[i]); i++) {
                // eat
            }
            // eat until white
        }
    }

    return ret;
}

double parse_date_string(std::string s) {
    std::regex date_regex{R"((\d\d\d\d)/(\d\d)/(\d\d) (\d\d):(\d\d):(\d\d)\.(\d\d\d) UTC)"};
    std::smatch m;

    if (std::regex_match(s, m, date_regex)) {
        struct tm then{};
        then.tm_year = std::stoi(m[1].str()) - 1900;
        then.tm_mon = std::stoi(m[2].str()) - 1;
        then.tm_mday = std::stoi(m[3].str());
        then.tm_hour = std::stoi(m[4].str());
        then.tm_min = std::stoi(m[5].str());
        then.tm_sec = std::stoi(m[6].str());

        time_t then_time = timegm(&then);

        return then_time + (std::stoi(m[7].str()) / 1000.0);
    }

    throw std::runtime_error{"can't parse date string: " + s};
}

void ptk_client::process(const char* str, size_t len) {
    try {
        tinyxml2::XMLDocument xml;
        tinyxml2::XMLError error = xml.Parse(str, len);
        if (error != 0) {
            throw std::runtime_error{"Could not parse XML document"};
        }

        auto config = config_get();

        if (auto event = xml.FirstChildElement("Event")) {
            if (event->FirstChildElement("ACK")) {
                return;
            }

            if (auto CallInfo = event->FirstChildElement("CallInfo")) {
                auto StreamInfo = CallInfo->FirstChildElement("StreamInfo");
                auto CallData = CallInfo->FirstChildElement("CallData");
                if (StreamInfo && CallData) {
                    std::string streamid;
                    if (auto id = StreamInfo->FirstChildElement("StreamID")) {
                        streamid = id->Attribute("ID");
                    }


                    std::set<int> positions = parse_position_string(CallData->Attribute("PositionsActive"));
                    std::set<int> on_hold = parse_position_string(CallData->Attribute("PositionsOnHold"));

                    if (!config->ptk_ignore_on_hold()) {
                        for (auto& x: on_hold) {
                            positions.erase(x);
                        }
                    }

                    double event_time = -std::numeric_limits<double>::infinity();

                    try {
                        event_time = parse_date_string(CallData->Attribute("EventDateTime"));
                    } catch (const std::exception& e) {
                        IRR_ERR("Can not parse EventDateTime: " << e.what());
                    }

                    if (streamid != "") {
                        impl_->notify_func(streamid, event_time, positions);
                    }
                }
            }
        }

        if (config->log_ptk_xml()) {
            tinyxml2::XMLPrinter print;
            xml.Print(&print);
            IRR_DEBUG("Received PTK XML:\n" << print.CStr());
        }
    } catch(const std::exception& e) {
        IRR_ERR("Could not process PTK XML message: " << e.what() << "\n" << (std::string{str, len}));
    }
}

void ptk_client::set_notify(std::function<void(std::string,double,std::set<int>)> func) {
    impl_->notify_func = std::move(func);
}

void ptk_client::connect_cycle() {
    if (impl_->connecting)
        return;

    impl_->connecting = true;
    auto config = config_get();

    auto finally_func = [self = shared_from_this(), this] () {
        if (!impl_->socket) {
            impl_->connectTimer.expires_from_now(std::chrono::seconds(1));
            impl_->connectTimer.async_wait(std::bind(&ptk_client::connect_cycle, shared_from_this()));
        }

        impl_->connecting = false;
    };

    if (config->ptk_address() && config->ptk_port() && config->ptk_position()) {
        auto resolver = std::make_shared<boost::asio::ip::tcp::resolver>(impl_->io_service);
        auto query = std::make_shared<boost::asio::ip::tcp::resolver::query>(*config->ptk_address(), std::to_string(*config->ptk_port()));

        resolver->async_resolve(*query, [self = shared_from_this(), this, finally_func, config, query, resolver]
        (const boost::system::error_code& error1, boost::asio::ip::tcp::resolver::iterator endpoint_iterator) {
            if (error1) {
                global_irr_status->set_alarm("ptk_client", alarm_priority::error, "Could not resolve Controller connection", error1.message());
                finally_func();
            } else {
                auto ptk_socket = std::make_shared<boost::asio::ip::tcp::socket>(impl_->io_service);

                boost::asio::async_connect(*ptk_socket, endpoint_iterator, [self = shared_from_this(), this, ptk_socket, finally_func, config, query, resolver]
                (const boost::system::error_code& error, boost::asio::ip::tcp::resolver::iterator) {
                    if (error) {
                        global_irr_status->set_alarm("ptk_client", alarm_priority::error, "Broken Controller connection", error.message());
                    } else {
                        impl_->socket = ptk_socket;
                        impl_->position = *config->ptk_position();
                        read_cycle();
                        global_irr_status->clear("ptk_client");

                        tinyxml2::XMLPrinter init;
                        init.PushHeader(false, true);
                        init.OpenElement("Event");
                        init.PushAttribute("id", "");
                        init.PushAttribute("XMLspec", "6.76");
                        init.PushAttribute("WindowsUser", "Instant Recall Recorder");
                        init.OpenElement("SendInitializationXML");
                        init.PushAttribute("ListenPort", "8080"); // todo: fix
                        init.PushAttribute("Position", std::to_string(impl_->position).c_str());
                        init.CloseElement();
                        init.CloseElement();

                        send(init.CStr());
                    }
                    finally_func();
                });
            }
        });
    } else {
        global_irr_status->clear("ptk_client");
    }
}
