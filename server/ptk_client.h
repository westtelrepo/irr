#ifndef WESTTEL_IRR_PTK_CLIENT
#define WESTTEL_IRR_PTK_CLIENT

#include <boost/asio.hpp>
#include <memory>
#include <set>

class ptk_client : public std::enable_shared_from_this<ptk_client> {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;

    ptk_client(boost::asio::io_service& io_service);

    void send(std::string);
    void read_cycle();
    void send_cycle();

    void heartbeat();

    void connect_cycle();

    void process(const char* str, size_t len);
public:
    template<class... Ts>
    static std::shared_ptr<ptk_client> create(Ts&&... ts) {
        return std::shared_ptr<ptk_client>{ new ptk_client{std::forward<Ts>(ts)...} };
    }

    void start();

    void set_notify(std::function<void(std::string,double,std::set<int>)>);
};

// for testing only
std::set<int> parse_position_string(std::string s, char type = 'P');
double parse_date_string(std::string s);

#endif
