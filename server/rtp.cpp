#include "../common/opus.hpp"
#include "config.h"
#include "logger.h"
#include "rtp.h"

#include <boost/optional.hpp>
#include <iomanip>
#include <jrtplib3/rtppacketbuilder.h>
#include <mutex>
#include <queue>
#include <set>
#include <thread>

std::set<int> positions_at_time(std::map<double, std::set<int>>& positions, double time, double offset) {
    std::set<int> ret;

    for (auto elem = positions.begin(); elem != positions.end(); ) {
        std::pair<double, double> interval;

        interval.first = elem->first;

        if (std::next(elem) != positions.end()) {
            interval.second = std::next(elem)->first;
        } else {
            interval.second = std::numeric_limits<double>::infinity();
        }

        interval.first -= offset;
        interval.second += offset;

        if (time >= interval.first && time <= interval.second) {
            for (auto& x: elem->second) {
                ret.insert(x);
            }
        }

        // if time is past the end of interval, interval will never be needed again
        // (assuming that time is unidirectional)
        if (time > interval.second) {
            elem = positions.erase(elem);
        } else {
            ++elem;
        }
    }

    return ret;
}

uint8_t ulaw_encode(int16_t sample) {
    const int16_t bias = 0x84;
    const int clip = 32635;
    static const uint8_t ulaw_table[256] = {
        0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,
        4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
        5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7
    };

    const int16_t sign = (sample >> 8) & 0x80;
    if (sign) {
        sample = -sample;
    }
    if (sample > clip) {
        sample = clip;
    }
    sample = sample + bias;
    const int16_t exponent = ulaw_table[(sample >> 7) & 0xFF];
    const int16_t mantissa = (sample >> (exponent+3)) & 0x0F;

    return static_cast<uint8_t>(~(sign | (exponent << 4) | mantissa));
}

uint8_t alaw_encode(int16_t sample) {
    const int clip = 32635;
    static const uint8_t alaw_table[128] = {
        1,1,2,2,3,3,3,3,
        4,4,4,4,4,4,4,4,
        5,5,5,5,5,5,5,5,
        5,5,5,5,5,5,5,5,
        6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,
        6,6,6,6,6,6,6,6,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7,
        7,7,7,7,7,7,7,7
    };

    const int sign = ((~sample) >> 8) & 0x80;
    if (!sign) {
        sample = -sample;
    }

    if (sample > clip) {
        sample = clip;
    }

    uint8_t ret;

    if (sample >= 256) {
        const int16_t exponent = alaw_table[(sample >> 8) & 0x7F];
        const int16_t mantissa = (sample >> (exponent + 3) ) & 0x0F;
        ret = static_cast<uint8_t>((exponent << 4) | mantissa);
    } else {
        ret = static_cast<uint8_t>(sample >> 4);
    }
    ret ^= (sign ^ 0x55);
    return ret;
}

static double convert_utc_to_monotonic(double unix_time) {
    timespec t{};
    int e = clock_gettime(CLOCK_REALTIME, &t);
    if (e != 0) {
        throw std::runtime_error{"Could not get CLOCK_REALTIME: " + std::string{strerror(errno)}};
    }
    double utc_now = t.tv_sec + 1e-9 * t.tv_nsec;

    int e2 = clock_gettime(CLOCK_MONOTONIC, &t);
    if (e2 != 0) {
        throw std::runtime_error{"Could not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
    }
    double mono_now = t.tv_sec + 1e-9 * t.tv_nsec;

    return unix_time + (mono_now - utc_now);
}

struct stream {
    std::string streamid;
    std::queue<std::pair<uint64_t, std::vector<char>>> queue;
    uint64_t start_time; // CLOCK_MONOSPACE quantized to 20ms

    std::map<double, std::set<int>> positions;

    bool to_stop = false;

    opus::Decoder decode{8000, 1};

    double offset; // from global rtp offset config

    bool before_known = false;

    // now is CLOCK_MONOSPACE quantized to 20ms
    boost::optional<std::vector<int16_t>> pop_if_time(uint64_t now) {
        while (queue.size() && queue.front().first + start_time < now) {
            queue.pop();
        }

        if (queue.size() && queue.front().first + start_time == now) {
            auto& opus = queue.front();

            std::vector<int16_t> pcm;
            pcm.resize(160); // 160 samples in 20 ms 8kHZ, 1 channel

            try {
                decode.decode(opus.second.data(), opus.second.size(), pcm.data(), static_cast<int>(pcm.size()));
            } catch(const std::exception& e) {
                IRR_ERR("Error decoding OPUS for RTP, ignoring: " << e.what());
                queue.pop();
                return boost::optional<std::vector<int16_t>>{};
            }

            queue.pop();
            return pcm;
        } else {
            return boost::optional<std::vector<int16_t>>{};
        }
    }

    // "done" if no more pieces and flag is set
    // or if "before_known" (i.e. we didn't see a stream) and the time has passed
    bool done(uint64_t now) { return (to_stop && !queue.size()) || (before_known && now >= start_time); }

    // time is according to CLOCK_MONOTONIC
    std::set<int> positions_at_time(double time) {
        auto config = config_get();
        auto ret = ::positions_at_time(positions, time, config->rtp_position_uncertainty());
        ret.insert(0);
        return ret;
    }
};

static void delete_packet_builder(jrtplib::RTPPacketBuilder* packet) {
    if (packet)
        packet->Destroy();
    delete packet;
}

struct endpoint {
private:
    std::unique_ptr<jrtplib::RTPPacketBuilder, void(*)(jrtplib::RTPPacketBuilder*)> rtp{nullptr, delete_packet_builder};
public:
    boost::asio::ip::udp::endpoint udp_endpoint;
    rtp_format format;
    double volume;

    // data is 20ms PCM piece, 8kHZ 1-channel
    void send(boost::asio::ip::udp::socket& socket, const std::vector<int16_t>& pcm) {
        assert(pcm.size() == 160);

        if (!rtp) {
            rtp = std::unique_ptr<jrtplib::RTPPacketBuilder, void(*)(jrtplib::RTPPacketBuilder*)>{
                new jrtplib::RTPPacketBuilder{*jrtplib::RTPRandom::CreateDefaultRandomNumberGenerator()},
                delete_packet_builder
            };
            rtp->Init(1480);
        }

        switch (format) {
            case rtp_format::BARIX_PCM_8kHZ_104: {
                auto ret = rtp->BuildPacket(pcm.data(), sizeof(int16_t) * pcm.size(), 104, false, 160);
                if (ret < 0) {
                    IRR_WARN(jrtplib::RTPGetErrorString(ret));
                    return;
                }
                break;
            }
            case rtp_format::ULAW_8kHZ_0: {
                std::vector<uint8_t> ulaw;
                ulaw.reserve(160);

                for (int16_t i: pcm) {
                    ulaw.push_back(ulaw_encode(i));
                }

                auto ret = rtp->BuildPacket(ulaw.data(), ulaw.size(), 0, false, 160);
                if (ret < 0) {
                    IRR_WARN(jrtplib::RTPGetErrorString(ret));
                    return;
                }
                break;
            }
            case rtp_format::ALAW_8kHZ_8: {
                std::vector<uint8_t> alaw;
                alaw.reserve(160);

                for (int16_t i: pcm) {
                    alaw.push_back(alaw_encode(i));
                }

                auto ret = rtp->BuildPacket(alaw.data(), alaw.size(), 8, false, 160);
                if (ret < 0) {
                    IRR_WARN(jrtplib::RTPGetErrorString(ret));
                    return;
                }
                break;
            }
        }

        try {
            auto buffer = std::make_shared<std::vector<uint8_t>>();
            buffer->resize(rtp->GetPacketLength());
            std::copy_n(rtp->GetPacket(), rtp->GetPacketLength(), buffer->begin());

            socket.async_send_to(boost::asio::buffer(*buffer), udp_endpoint, [buffer] (const boost::system::error_code& error, size_t) {
                if (error) {
                    IRR_WARN("Error sending UDP packet: " << error.message());
                }
            });
        } catch(const std::exception& e) {
            IRR_DEBUG("Exception sending UDP packet: " << e.what());
        }
    }

    void reset() {
        rtp.reset();
    }
};

struct rtp::impl {
    boost::asio::ip::udp::socket socket;

    std::thread thread;

    std::mutex mutex; // owns following variables

    // streamid -> stream
    std::map<std::string, stream> streams;
    // position -> endpoint
    std::map<int, endpoint> endpoints;

    impl(boost::asio::io_service& io_service)
    : socket{io_service} {
        boost::asio::ip::udp::resolver resolver{io_service};
        socket.open(boost::asio::ip::udp::v4());
    }

    std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)> config_update;
};

rtp::rtp(boost::asio::io_service& io)
: impl_{new impl{io}, [] (impl* i) { delete i; }} {
}

void rtp::start() {
    auto config = config_get();

    auto endpoints = config->rtp_endpoints();
    for (auto& end: endpoints) {
        endpoint e{};
        e.udp_endpoint = boost::asio::ip::udp::endpoint{end.second.address, end.second.port};
        e.format = end.second.format;
        e.volume = end.second.volume;
        impl_->endpoints.emplace(end.first, std::move(e));
    }

    impl_->config_update = [this] (std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config> new_config) {
        std::lock_guard<std::mutex> lock_guard{impl_->mutex};

        auto new_end = new_config->rtp_endpoints();

        // erase endpoints that aren't in the new config
        for (auto end = impl_->endpoints.begin(); end != impl_->endpoints.end(); ) {
            if (!new_end.count(end->first)) {
                end = impl_->endpoints.erase(end);
            } else {
                ++end;
            }
        }

        for (auto& end: new_end) {
            if (!impl_->endpoints.count(end.first)) {
                endpoint e{};
                e.udp_endpoint = boost::asio::ip::udp::endpoint{end.second.address, end.second.port};
                e.format = end.second.format;
                e.volume = end.second.volume;
                impl_->endpoints.emplace(end.first, std::move(e));
            } else {
                impl_->endpoints[end.first].udp_endpoint = boost::asio::ip::udp::endpoint{end.second.address, end.second.port};
                impl_->endpoints[end.first].format = end.second.format;
                impl_->endpoints[end.first].volume = end.second.volume;
            }
        }
    };

    config_subscribe(
        std::shared_ptr<
            std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)>
        >{shared_from_this(), &impl_->config_update}
    );

    impl_->thread = std::thread{std::bind(&rtp::exec, shared_from_this())};
}

void rtp::start(std::string streamid, double unix_time) {
    std::lock_guard<std::mutex> lock_guard{impl_->mutex};

    timespec t{};
    int e = clock_gettime(CLOCK_REALTIME, &t);
    if (e != 0) {
        throw std::runtime_error{"Could not get CLOCK_REALTIME: " + std::string{strerror(errno)}};
    }
    double now = t.tv_sec + 1e-9 * t.tv_nsec;

    IRR_DEBUG("start " << streamid << " with " << now - unix_time);

    stream s{};

    if (impl_->streams.count(streamid)) {
        s.positions = std::move(impl_->streams[streamid].positions);
        impl_->streams.erase(streamid);
    }
    s.streamid = streamid;
    s.offset = config_get()->rtp_offset();
    s.start_time = static_cast<uint64_t>(50 * s.offset + 50 * convert_utc_to_monotonic(unix_time));

    impl_->streams.emplace(streamid, std::move(s));
}

void rtp::send(std::string streamid, uint64_t piece_num, std::vector<char> data) {
    std::lock_guard<std::mutex> lock_guard{impl_->mutex};
    assert(impl_->streams.count(streamid));
    impl_->streams[streamid].queue.emplace(std::make_pair(piece_num, std::move(data)));
}

void rtp::stop(std::string streamid) {
    std::lock_guard<std::mutex> lock_guard{impl_->mutex};
    if (impl_->streams.count(streamid)) {
        impl_->streams[streamid].to_stop = true;
    }
}

void rtp::notify(std::string streamid, double unix_time, std::set<int> positions) {
    std::string pos;
    for (auto x: positions)
        pos += std::to_string(x) + " ";
    pos = "(" + pos + ")";

    std::lock_guard<std::mutex> lock_guard{impl_->mutex};
    if (impl_->streams.count(streamid)) {
        impl_->streams[streamid].positions[convert_utc_to_monotonic(unix_time) + impl_->streams[streamid].offset] = positions;
        IRR_DEBUG("notify " << streamid << " " << std::fixed << std::setprecision(3) << unix_time << " " << pos);
    } else {
        timespec t{};
        int e = clock_gettime(CLOCK_MONOTONIC, &t);
        if (e != 0) {
            throw std::runtime_error{"Could not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
        }

        stream s{};
        s.streamid = streamid;
        // wait for offset + 20 seconds to kill it
        s.start_time = static_cast<uint64_t>(50 * config_get()->rtp_offset() + 50 * (t.tv_sec + 1e-9 * t.tv_nsec) + 50 * 20);
        s.before_known = true;
        s.positions[convert_utc_to_monotonic(unix_time)] = positions;

        impl_->streams.emplace(streamid, std::move(s));
        IRR_DEBUG("notify w/o seeing stream " << streamid << " " << std::fixed << std::setprecision(3) << unix_time << " " << pos);
    }
}

// clamp the edges, don't handle NaN
static int16_t to_int16(double d) {
    if (d > INT16_MAX)
        return INT16_MAX;
    else if (d < INT16_MIN)
        return INT16_MIN;
    else
        return static_cast<int16_t>(d);
}

// this is the RTP thread
void rtp::exec() {
    uint64_t processed_time;
    {
        timespec t{};
        int e = clock_gettime(CLOCK_MONOTONIC, &t);
        if (e != 0) {
            IRR_CRIT("Can not get CLOCK_MONOTONIC: " << strerror(errno));
            // yes this throw will kill the program
            throw std::runtime_error{"Can not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
        }
        processed_time = 50 * static_cast<uint64_t>(t.tv_sec) + static_cast<uint64_t>(t.tv_nsec) / 20000000;
    }

    while(true) {
        {
            // sleep until the next 20ms "tick"
            timespec sleep_until{};
            sleep_until.tv_sec = static_cast<time_t>((processed_time + 1) / 50);
            sleep_until.tv_nsec = ((processed_time + 1) % 50) * 20000000;
            int e = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &sleep_until, nullptr);

            if (e != 0 && errno != EINTR) {
                throw std::runtime_error{"clock_nanosleep failed: " + std::string{strerror(errno)}};
            }
        }

        timespec before_lock{};
        int e1 = clock_gettime(CLOCK_MONOTONIC, &before_lock);
        if (e1 != 0) {
            throw std::runtime_error{"Could not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
        }

        std::lock_guard<std::mutex> lock_guard{impl_->mutex};

        timespec t{};
        int e2 = clock_gettime(CLOCK_MONOTONIC, &t);
        if (e2 != 0) {
            throw std::runtime_error{"Could not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
        }
        uint64_t now = 50 * static_cast<uint64_t>(t.tv_sec) + static_cast<uint64_t>(t.tv_nsec) / 20000000;

        auto config = config_get();

        double after_lock = t.tv_sec + 1e-9 * t.tv_nsec;
        double before_lock_time = before_lock.tv_sec + 1e-9 * before_lock.tv_nsec;

        if (after_lock - before_lock_time > config->rtp_thread_lock_sec()) {
            IRR_WARN("RTP thread took " << 1000 * (after_lock - before_lock_time) << " ms to lock mutex");
        }

        // log if skipped so many RTP frames
        if (now - processed_time > config->log_missed_frames()) {
            IRR_WARN("Skipped " << (now - processed_time) << " number of RTP frames, which is " << (now - processed_time) * 20 << " ms");
        }

        // 200 frames is 4 seconds. If we have waited this long something terrible
        // has happened (like SIGSTOP)
        if (now - processed_time > 200) {
            IRR_CRIT("Skipped " << (now - processed_time) << " number of RTP frames, ignoring intervening time");
            processed_time = now;
        }

        double comfort_level = config->rtp_comfort_level();

        // process each tick until we get to the current tick
        for (; processed_time <= now; ++processed_time) {
            std::map<int, std::vector<int16_t>> endpoint_to_pcm;

            for (auto stream = impl_->streams.begin(); stream != impl_->streams.end();) {
                if (auto pcm = stream->second.pop_if_time(processed_time)) {
                    auto positions = stream->second.positions_at_time(processed_time * 0.02);
                    for (auto& position: positions) {
                        if (endpoint_to_pcm.count(position)) {
                            // if there is already PCM data for an endpoint, mix this in
                            auto& p = endpoint_to_pcm[position];
                            for (size_t i = 0; i < pcm->size(); ++i) {
                                // if people are extremely loud this will cause clipping but there
                                // isn't much we can do
                                p[i] = to_int16(static_cast<double>(p[i]) + pcm.value()[i]);
                            }
                        } else {
                            endpoint_to_pcm[position] = *pcm;
                        }
                    }
                }

                if (stream->second.done(now)) {
                    stream = impl_->streams.erase(stream);
                } else {
                    ++stream;
                }
            }

            for (auto& x: endpoint_to_pcm) {
                if (impl_->endpoints.count(x.first)) {
                    auto& endpoint = impl_->endpoints[x.first];

                    for (size_t i = 0; i < x.second.size(); i++) {
                        x.second[i] = to_int16(endpoint.volume * x.second[i]);
                    }

                    auto detection_level = config->rtp_detection_level();

                    bool add_noise = false;
                    if (detection_level) {
                        double avg = 0;
                        for (size_t i = 0; i < x.second.size(); i++) {
                            avg += std::fabs(x.second[i]);
                        }
                        avg /= x.second.size();

                        if (avg <= *detection_level) {
                            add_noise = true;
                        }
                    } else {
                        add_noise = true;
                    }

                    if (add_noise) {
                        // add white noise for each endpoint
                        for (size_t i = 0; i < x.second.size(); i++) {
                            x.second[i] = to_int16(x.second[i] + (comfort_level / 2.0 - comfort_level * (static_cast<double>(rand()) / RAND_MAX)));
                        }
                    }

                    // play the endpoint
                    endpoint.send(impl_->socket, x.second);
                }
            }

            if (config->rtp_always_play() || !impl_->streams.empty()) {
                // for the endpoints that we didn't get sound for on this tick, play pure silence
                for (auto& x: impl_->endpoints) {
                    if (!endpoint_to_pcm.count(x.first)) {
                        std::vector<int16_t> empty_pcm;
                        empty_pcm.resize(160, 0);
                        x.second.send(impl_->socket, empty_pcm);
                    }
                }
            }

            if (!config->rtp_always_play() && impl_->streams.empty()) {
                for (auto& x: impl_->endpoints) {
                    x.second.reset();
                }
            }
        }

        timespec time_after{};
        int e3 = clock_gettime(CLOCK_MONOTONIC, &time_after);
        if (e3 != 0) {
            throw std::runtime_error{"Could not get CLOCK_MONOTONIC: " + std::string{strerror(errno)}};
        }

        double time_after_time = time_after.tv_sec + 1e-9 * time_after.tv_nsec;
        if (time_after_time - after_lock > config->rtp_ran_for_sec()) {
            IRR_WARN("RTP thread ran for " << (time_after_time - after_lock) * 1000.0 << " ms");
        }

    }
}
