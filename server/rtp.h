#ifndef WESTTEL_RTP
#define WESTTEL_RTP

#include <boost/asio.hpp>
#include <map>
#include <memory>
#include <set>
#include <vector>

class rtp : public std::enable_shared_from_this<rtp> {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;

    [[noreturn]] void exec();
public:
    rtp(boost::asio::io_service&);

    void start();

    void start(std::string streamid, double unix_time);
    void send(std::string streamid, uint64_t piece_num, std::vector<char> opus);
    void stop(std::string streamid);

    void notify(std::string streamid, double unix_time, std::set<int> positions);
};

// for testing
// BEWARE: deletes no-longer needed information
std::set<int> positions_at_time(std::map<double, std::set<int>>& positions, double time, double offset = 0);

// for testing
uint8_t ulaw_encode(int16_t sample);
uint8_t alaw_encode(int16_t sample);

#endif
