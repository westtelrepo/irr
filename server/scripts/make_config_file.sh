#!/bin/bash

# TODO: not die if given absolute path for some reason
FILE="$PWD/$1"

pushd "$2" > /dev/null

set -e

if [ -n "`git status --porcelain`" ]; then
    GIT_DIRTY="true";
    BUILD_TIME="$(date --utc +%s -d "$(date --utc -Ihour)")"
else
    GIT_DIRTY="false";
    BUILD_TIME="$(git show -s --format=%ct)"
fi

# TODO: could probably get rid of temporary file

TMP_FILE="/tmp/8a9c7387-914c-464c-9092-b6e7ed4036da-git.cpp"

# we only set the timestamp to the nearest hour so that
# ninja doesn't need to continuously link forever
# (below in BUILD_TIME)
cat > "$TMP_FILE" <<HERE
#include <time.h>
extern const char* GIT_HASH;
extern const char* GIT_HASH_SHORT;
extern time_t BUILD_TIME;
extern const char* BUILD_LSB_DESC;
extern bool GIT_DIRTY;

const char* GIT_HASH = "$(git rev-parse --verify HEAD)";
const char* GIT_HASH_SHORT = "$(git rev-parse --verify --short HEAD)";
time_t BUILD_TIME = $BUILD_TIME;
const char* BUILD_LSB_DESC = "$(lsb_release --short --description)";
bool GIT_DIRTY = $GIT_DIRTY;
HERE

# if we don't update the file, we tell ninja that it hasn't changed
# and it won't cause a rebuild
# so we only overwrite on change
# (also overwrites on diff error, which is what we want)
if ! diff "$TMP_FILE" "$FILE" > /dev/null 2> /dev/null; then
    cat "$TMP_FILE" > "$FILE"
fi

rm "$TMP_FILE"

popd > /dev/null
