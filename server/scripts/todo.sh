#!/bin/bash

find $MESON_SOURCE_ROOT -type f -print0 | while IFS= read -r -d $'\0' file; do
    # the weird quote thing is so we don't match ourselves
    fgrep -Hn --color=auto -e 'TO'DO -e 'FIX'ME $file;
done

true
