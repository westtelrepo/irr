#include "status.h"

#include <algorithm>
#include <functional>
#include <limits.h>
#include <map>
#include <mutex>
#include <sstream>
#include <systemd/sd-daemon.h>
#include <thread>
#include <unistd.h>
#include <vector>

#include "config.h"
#include "esmtp.hpp"
#include "globals.h"
#include "logger.h"

static void send_email(std::string subject, std::string body, std::function<void()> if_success = [] () {}) {
    std::thread thread{[subject, body, if_success] () {
        try {
            auto config = config_get();

            if (!config->email_recipients().size())
                return;
            if (!config->alerts_active())
                return;

            // create the message body before creating the session, so
            // it will be around after the message is ended
            const std::string message_body = esmtp::message::body_to_message(body);
            const auto recipients = config->email_recipients();
            const std::string phrase = config->email_from_phrase();
            const std::string address = config->email_from_address();

            esmtp::session session;
            session.set_server(config->email_server().c_str());
            auto message = session.add_message();

            for (auto& recipient: recipients) {
                message.add_recipient(recipient.c_str());
            }

            message.set_subject(subject.c_str());
            message.set_from(phrase.c_str(), address.c_str());
            message.set_message(message_body.c_str());

            session.start();

            auto status = global_irr_status;
            if (status)
                status->clear("email");

            IRR_INFO("Sent email: " << subject);

            if_success();
        } catch(const std::exception& e) {
            IRR_ERR(std::string{"Unhandled exception in sending email: "} + e.what());
            auto status = global_irr_status;
            if (status)
                status->set_alarm("email", alarm_priority::error, "Unhandled exception in sending email (dropped)", e.what());
        }
    }};

    thread.detach();
}

struct alarm_type {
    time_t t;
    std::string alarm_name;
    std::string alarm_subname;
    std::string alarm_title;
    std::string message;
    alarm_priority priority;

    bool is_same(const alarm_type& other) {
        return alarm_name == other.alarm_name &&
            alarm_subname == other.alarm_subname &&
            alarm_title == other.alarm_title &&
            priority >= other.priority;
    }
};

struct irr_status::impl {
    std::mutex mutex;
    std::map<std::string, std::map<std::string, alarm_type>> alarms;
    bool started = false;

    time_t sent = 0;
    alarm_priority sent_priority = alarm_priority::no_error;

    time_t last_reminder = 0;

    time_t all_clear = 0;
    bool sent_all_clear = true;

    void escalate() {
        time_t now = time(nullptr);
        auto config = config_get();
        time_t sec = config->warning_escalate_sec();
        for (auto& set : alarms) {
            for (auto& a: set.second) {
                if (a.second.priority == alarm_priority::warning && a.second.t < now + sec) {
                    a.second.priority = alarm_priority::error;
                }
            }
        }
    }

    alarm_type highest() const {
        alarm_type ret;
        ret.priority = alarm_priority::no_error;

        for (auto& set: alarms) {
            for (auto& a: set.second) {
                if (a.second.priority > ret.priority)
                    ret = a.second;
            }
        }

        return ret;
    }
};

irr_status::irr_status(): impl_{ new impl{}, [](impl* i){ delete i; } } {

}

void irr_status::clear(std::string alarm_name, std::string subname) {
    std::lock_guard<std::mutex> guard{impl_->mutex};
    if (impl_->alarms.count(alarm_name)) {
        if (impl_->alarms[alarm_name].count(subname)) {
            alarm_type a = impl_->alarms[alarm_name][subname];
            impl_->alarms[alarm_name].erase(subname);
            IRR_NOTICE("Cleared alarm: " << a.alarm_title << ": " << a.message);
            if (impl_->alarms[alarm_name].size() == 0) {
                impl_->alarms.erase(alarm_name);

                // if alarms is now 0 we just cleared all alarms
                if (impl_->alarms.size() == 0) {
                    impl_->sent_all_clear = false;
                    impl_->all_clear = time(nullptr);
                }
            }
        }
    }

    int_set_systemd();
}

void irr_status::set_alarm(
    std::string alarm_name, std::string subname, alarm_priority priority, std::string alarm_title, std::string message) {
    {
        std::lock_guard<std::mutex> guard{impl_->mutex};

        alarm_type a;
        a.t = time(nullptr);
        a.alarm_name = alarm_name;
        a.alarm_subname = subname;
        a.alarm_title = alarm_title;
        a.priority = priority;
        a.message = message;

        alarm_type& old = impl_->alarms[alarm_name][subname];
        if (!old.is_same(a)) {
            impl_->alarms[alarm_name][subname] = a;
            switch(priority) {
                case alarm_priority::no_error:
                    break;
                case alarm_priority::warning:
                    IRR_WARN(alarm_title << ": " << message);
                    break;
                case alarm_priority::error:
                case alarm_priority::other_error:
                    IRR_ERR(alarm_title << ": " << message);
                    break;
                case alarm_priority::programming_error:
                    IRR_CRIT(alarm_title << ": " << message << "\nstarting less graceful shutdown in an attempt to recover");
                    global_graceful_shutdown = 2;
                    break;
            }
        } else {
            // update message, don't send mail
            // since title didn't change
            // but record new message for status
            old.message = message;
        }
    }

    // notify immediately so we can send message if needed
    // also do it outside the lock_guard because it has its own
    notify();
}

std::string irr_status::string_status() const {
    std::lock_guard<std::mutex> guard{impl_->mutex};
    return this->int_string_status();
}

std::string irr_status::int_string_status() const {
    std::vector<std::vector<alarm_type>> to_print;
    to_print.reserve(impl_->alarms.size());

    for (const auto& alarm_set: impl_->alarms) {
        std::vector<alarm_type> set;
        set.reserve(alarm_set.second.size());
        for (const auto& alarm: alarm_set.second) {
            set.push_back(alarm.second);
        }

        // put highest priority first
        std::stable_sort(set.begin(), set.end(), [] (const alarm_type& a, const alarm_type& b) {
            return a.priority > b.priority;
        });

        // set.size() should never legitimately be zero, but avoid adding it so that
        // the next sort will work
        if (set.size() != 0) {
            // avoid copying the vector
            to_print.emplace_back(std::move(set));
        }
    }

    // put the highest priority on top
    std::stable_sort(to_print.begin(), to_print.end(), [] (const std::vector<alarm_type>& a, const std::vector<alarm_type>& b) {
        // since each array is sorted with max priority at the front,
        // we can just compare the fronts
        // also, a and b will never be empty (so 0 will always be valid)
        return a.at(0).priority > b.at(0).priority;
    });

    std::ostringstream ss;
    if (to_print.size() == 0) {
        ss << "No current alarms" << std::endl;
    } else {
        ss << "All current alarms:" << std::endl;
    }
    for (const auto& alarm_set: to_print) {
        ss << "    ";
        // only print most important subalarms for each alarm
        const size_t MAX_ALARMS = 25;
        for (size_t i = 0; i < std::min(alarm_set.size(), MAX_ALARMS); i++) {
            if (i != 0)
                ss << "    ";
            // TODO: better message output (sometimes has newlines)
            ss << alarm_set[i].alarm_title << ": " << alarm_set[i].message << std::endl;
            if (i == MAX_ALARMS - 1)
                ss << "[WARNING] " << (alarm_set.size() - MAX_ALARMS) << " similar alarms omitted" << std::endl;
        }
        ss << std::endl;
    }

    return ss.str();
}

void irr_status::int_set_systemd() const {
    alarm_type max_priority;
    max_priority.priority = alarm_priority::no_error;
    for (const auto& set: impl_->alarms) {
        for (const auto& alarm: set.second) {
            if (alarm.second.priority > max_priority.priority)
                max_priority = alarm.second;
        }
    }

    if (max_priority.priority != alarm_priority::no_error) {
        std::string first_line = max_priority.message.substr(0, max_priority.message.find('\n'));
        std::string status = max_priority.alarm_title + ": " + first_line;
        sd_notify(0, ("STATUS=" + status).c_str());
    } else if (!impl_->started) {
        sd_notify(0, "STATUS=Starting IRR");
    } else if (global_graceful_shutdown == 1) {
        sd_notify(0, "STATUS=Beginning graceful shutdown");
    } else if (global_graceful_shutdown == 2) {
        sd_notify(0, "STATUS=Beginning graceful shutdown, ignoring database");
    } else {
        sd_notify(0, "STATUS=IRR Running Green");
    }
}

void irr_status::notify() {
    std::lock_guard<std::mutex> guard{impl_->mutex};
    impl_->escalate();
    int_set_systemd();

    auto config = config_get();
    time_t now = time(nullptr);

    alarm_type current = impl_->highest();
    if (current.priority != alarm_priority::no_error && current.priority > impl_->sent_priority) {
        int_send_email(current.priority, current.alarm_title, current.message, current.t, [this, current, now] () {
            std::lock_guard<std::mutex> guard2{impl_->mutex};
            impl_->last_reminder = std::max(now, impl_->last_reminder);
            impl_->sent_priority = std::max(current.priority, impl_->sent_priority);
            impl_->sent = std::max(impl_->sent, now);
        });
    } else if (current.priority != alarm_priority::no_error && now > impl_->last_reminder + config->reminder_sec()) {
        int_send_email(current.priority, "[REMINDER] " + current.alarm_title, current.message, current.t, [this, now] () {
            std::lock_guard<std::mutex> guard2{impl_->mutex};
            impl_->last_reminder = std::max(now, impl_->last_reminder);
            impl_->sent = std::max(impl_->sent, now);
        });
    } else if (current.priority < impl_->sent_priority && now > impl_->sent + config->all_clear_sec()) {
        // de-escalate
        impl_->sent_priority = current.priority;
    } else if (current.priority == alarm_priority::no_error && !impl_->sent_all_clear && now > impl_->all_clear + config->all_clear_sec()) {
        int_send_email(alarm_priority::no_error, "ALL CLEAR", "All IRR alarms are clear", std::time(nullptr), [this] () {
            std::lock_guard<std::mutex> guard2{impl_->mutex};
            impl_->sent_all_clear = true;
            impl_->sent_priority = alarm_priority::no_error;
        });
    }
}

void irr_status::notify_started() {
    {
        std::lock_guard<std::mutex> guard{impl_->mutex};
        impl_->started = true;
    }

    if (auto config = config_get()) {
        if (config->email_startup()) {
            time_t now = time(nullptr);
            int_send_email(alarm_priority::no_error, "IRR Service Started", "IRR Service Stated", now);
        }
    }
    // Tell systemd that we are ready
	sd_notify(0, "READY=1");
    notify();
}

void irr_status::notify_reload() {
    if (auto config = config_get()) {
        if (config->email_reload()) {
            time_t now = time(nullptr);
            int_send_email(alarm_priority::no_error, "IRR Service Reloaded", "IRR Service Reloaded", now);
        }
    }
}

void irr_status::int_send_email(alarm_priority priority, const std::string& alarm_title,
    const std::string& message, time_t timestamp, std::function<void()> if_success) const {

    std::string prefix;
    if (priority == alarm_priority::no_error) {

    } else if (priority == alarm_priority::warning) {
        prefix = "[WARNING] ";
    } else {
        prefix = "[ALARM] ";
    }

    std::array<char, HOST_NAME_MAX> hostname;
    gethostname(hostname.data(), hostname.size());

    std::array<char, 128> timestamp_str;
    std::array<char, 128> timestamp_local_str;
    strftime(timestamp_str.data(), timestamp_str.size(), "%F %H:%M:%S %Z", gmtime(&timestamp));
    strftime(timestamp_local_str.data(), timestamp_local_str.size(), "%a %F %H:%M:%S %Z (%z)", localtime(&timestamp));

    send_email(prefix + "[IRR] " + alarm_title,
        "Alarm Text = " + message + "\n" +
        "Server     = " + hostname.data() + "\n" +
        "Time Stamp = " + timestamp_str.data() + " | " + timestamp_local_str.data() + "\n" +
        "\n" +
        this->int_string_status(),
        if_success
    );
}
