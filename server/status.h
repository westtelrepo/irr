#ifndef IRR_STATUS_HEADER
#define IRR_STATUS_HEADER

#include <memory>

enum class alarm_priority : int {
    // greater number is greater priority (aka down the list)
    // numbers are not set and are not external, aka
    // never use numbers always use names
    // however they may be compared
    no_error = 0,
    // warnings turn into errors after so many seconds
    warning,
    // "other_error" is less than an error
    other_error,
    error,
    programming_error,
};

class irr_status {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
    irr_status();

    // status reporting
    std::string string_status() const;

    void clear(std::string alarm_name, std::string subname = "");
    void set_alarm(std::string alarm_name, std::string subname, alarm_priority priority, std::string alarm_title, std::string message);

    void set_alarm(std::string alarm_name, alarm_priority priority, std::string alarm_title, std::string message) {
        this->set_alarm(alarm_name, "", priority, alarm_title, message);
    }

    void notify();
    void notify_started();
    void notify_reload();
private:
    std::string int_string_status() const;
    void int_set_systemd() const;
    void int_send_email(alarm_priority priority, const std::string& alarm_title,
        const std::string& message, time_t timestamp,
        std::function<void()> if_success = [] () {}) const;
};

#endif
