#include <catch.hpp>
#include <boost/optional/optional_io.hpp>

#include "../config.h"

#pragma clang diagnostic ignored "-Wfloat-equal"

class temp_env {
private:
    std::string name_;
public:
    temp_env(std::string name, std::string value): name_{name} {
        int e = setenv(name.c_str(), value.c_str(), 1);
        if (e) throw std::runtime_error{"setenv failed"};
    }

    ~temp_env() {
        unsetenv(name_.c_str());
    }
};

TEST_CASE("irr_config default constructor", "[irr_config]") {
    const irr_config config;

    REQUIRE(config.alerts_active() == false);
    REQUIRE(config.email_startup() == false);
    REQUIRE(config.email_reload() == false);
    REQUIRE(config.email_recipients().size() == 0);
    REQUIRE(config.email_server() == "localhost:25");
    REQUIRE(config.recording_dir() == "/var/opt/irr_recordings");
    REQUIRE(config.retention_interval() == std::chrono::seconds(3600));
    REQUIRE(config.retention_limit() == 500);
    REQUIRE(config.db_uri() == "");
    REQUIRE(config.port() == 6000);

    REQUIRE(!config.ptk_address());
    REQUIRE(!config.ptk_port());
    REQUIRE(!config.ptk_address());
    REQUIRE(config.ptk_ignore_on_hold() == false);

    REQUIRE(config.rtp_endpoints().size() == 0);
    REQUIRE(config.rtp_comfort_level() == 0.0);
    REQUIRE(!config.rtp_detection_level());
    REQUIRE(config.rtp_always_play() == false);
    REQUIRE(config.rtp_offset() > 0.5);
    REQUIRE(config.rtp_position_uncertainty() == 0);

    REQUIRE(config.log_level() == 7);
    REQUIRE(config.log_dest() == log_dest::Stdout);
    REQUIRE(config.log_ptk_xml() == false);
}

TEST_CASE("irr_config basic", "[irr_config]") {
    const irr_config config{R"(
---
alerts:
  active: on
  email:
    server: localhost:2525
    to:
      - vagrant@contrib-stretch.localdomain
)"};

    REQUIRE(config.alerts_active() == true);
    REQUIRE(config.email_recipients().size() == 1);
    REQUIRE(config.email_recipients().at(0) == "vagrant@contrib-stretch.localdomain");
    REQUIRE(config.email_server() == "localhost:2525");
}

TEST_CASE("irr_config email_from", "[irr_config]") {
    const irr_config config{R"(
---
alerts:
  email:
    from:
      address: irr@test.com
      phrase: IRR Test Site Alpha
)"};

    REQUIRE(config.email_from_address() == "irr@test.com");
    REQUIRE(config.email_from_phrase() == "IRR Test Site Alpha");
}

TEST_CASE("irr_config recording", "[irr_config]") {
    const irr_config config{R"(
---
recordings:
  dir: /tmp/what
)"};

    REQUIRE(config.recording_dir() == "/tmp/what");
}

TEST_CASE("irr config env", "[irr_config]") {
    temp_env e{"IRR_RECORDING_DIR", "/tmp/env"};
    const irr_config config;

    REQUIRE(config.recording_dir() == "/tmp/env");
}

TEST_CASE("irr config overrides env", "[irr_config]") {
    temp_env e{"IRR_RECORDING_DIR", "/tmp/env"};
    const irr_config config{R"(
recordings:
  dir: /tmp/file
)"};

    REQUIRE(config.recording_dir() == "/tmp/file");
}

TEST_CASE("irr alerts activated by default if alerts active", "[irr_config]") {
    const irr_config config{"alerts:\n  active: on\n"};

    REQUIRE(config.alerts_active() == true);
    REQUIRE(config.email_startup() == true);
    REQUIRE(config.email_reload() == true);
}

TEST_CASE("irr alerts can be turned off", "[irr_config]") {
    const irr_config config{R"(
alerts:
  active: on
  email:
    startup: false
    reload: no
)"};

    REQUIRE(config.alerts_active() == true);
    REQUIRE(config.email_startup() == false);
    REQUIRE(config.email_reload() == false);
}

TEST_CASE("warning_escalate_sec", "[irr_config]") {
    const irr_config config{R"(
alerts:
  email:
    warning_escalate_sec: 42
)"};

    REQUIRE(config.warning_escalate_sec() == 42);
}

TEST_CASE("reminder_sec", "[irr_config]") {
    const irr_config config{R"(
alerts:
  email:
    reminder_sec: 42
)"};

    REQUIRE(config.reminder_sec() == 42);
}

TEST_CASE("all_clear_sec", "[irr_config]") {
    const irr_config config{R"(
alerts:
  email:
    all_clear_sec: 42
)"};

    REQUIRE(config.all_clear_sec() == 42);
}

TEST_CASE("retention_limit env") {
    temp_env e{"IRR_RETENTION_LIMIT", "42"};
    const irr_config config;
    REQUIRE(config.retention_limit() == 42);
}

TEST_CASE("retention_interval env") {
    temp_env e{"IRR_RETENTION_INTERVAL", "42"};
    const irr_config config;
    REQUIRE(config.retention_interval() == std::chrono::seconds(42));
}

TEST_CASE("retention_limit, config overrides env") {
    temp_env e{"IRR_RETENTION_LIMIT", "42"};
    const irr_config config{R"(
db:
  retention:
    limit: 74
)"};
    REQUIRE(config.retention_limit() == 74);
}

TEST_CASE("retention_interval, config overrides env") {
    temp_env e{"IRR_RETENTION_INTERVAL", "42"};
    const irr_config config{R"(
db:
  retention:
    interval: 74
)"};
    REQUIRE(config.retention_interval() == std::chrono::seconds(74));
}

TEST_CASE("db_uri") {
    const irr_config config{R"(
db:
  uri: postgresql://something:password@hostname/database
)"};
    REQUIRE(config.db_uri() == "postgresql://something:password@hostname/database");
}

TEST_CASE("port") {
    const irr_config config{R"(
port: 42
)"};
    REQUIRE(config.port() == 42);
}

TEST_CASE("port env") {
    temp_env e{"IRR_PORT", "43"};
    const irr_config config;
    REQUIRE(config.port() == 43);
}

TEST_CASE("config file overrides port") {
    temp_env e{"IRR_PORT", "4333"};
    const irr_config config{"port: 42"};
    REQUIRE(config.port() == 42);
}

TEST_CASE("ptk connection") {
    const irr_config config{R"(
rtp:
  psap_toolkit:
    address: 10.10.10.10
    port: 5050
    position: 7
)"};

    REQUIRE(config.ptk_address().value() == "10.10.10.10");
    REQUIRE(config.ptk_port().value() == 5050);
    REQUIRE(config.ptk_position() == 7);
}

TEST_CASE("ptk ignore on hold") {
    const irr_config config{R"(
rtp:
  psap_toolkit:
    ignore_on_hold: true
)"};

    REQUIRE(config.ptk_ignore_on_hold() == true);
}

TEST_CASE("rtp endpoint") {
    const irr_config config{R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: BARIX_PCM_8kHZ_104
)"};

    REQUIRE(config.rtp_endpoints().size() == 1);
    REQUIRE(config.rtp_endpoints().count(6) == 1);
    REQUIRE(config.rtp_endpoints()[6].address == boost::asio::ip::address_v4::from_string("10.10.10.10"));
    REQUIRE(config.rtp_endpoints()[6].port == 4444);
    REQUIRE(config.rtp_endpoints()[6].position == 6);
    REQUIRE(config.rtp_endpoints()[6].format == rtp_format::BARIX_PCM_8kHZ_104);
}

TEST_CASE("rtp endpoint ulaw") {
    const irr_config config{R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: ULAW_8kHZ_0
)"};

    REQUIRE(config.rtp_endpoints().size() == 1);
    REQUIRE(config.rtp_endpoints().count(6) == 1);
    REQUIRE(config.rtp_endpoints()[6].address == boost::asio::ip::address_v4::from_string("10.10.10.10"));
    REQUIRE(config.rtp_endpoints()[6].port == 4444);
    REQUIRE(config.rtp_endpoints()[6].position == 6);
    REQUIRE(config.rtp_endpoints()[6].format == rtp_format::ULAW_8kHZ_0);
}

TEST_CASE("rtp endpoint volume") {
    const irr_config config{R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: ULAW_8kHZ_0
      volume: 0.47
)"};

    REQUIRE(config.rtp_endpoints().size() == 1);
    REQUIRE(config.rtp_endpoints().count(6) == 1);
    REQUIRE(config.rtp_endpoints()[6].address == boost::asio::ip::address_v4::from_string("10.10.10.10"));
    REQUIRE(config.rtp_endpoints()[6].port == 4444);
    REQUIRE(config.rtp_endpoints()[6].position == 6);
    REQUIRE(config.rtp_endpoints()[6].format == rtp_format::ULAW_8kHZ_0);
    REQUIRE(config.rtp_endpoints()[6].volume == 0.47);
}

TEST_CASE("rtp two endpoint volume") {
    const irr_config config{R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: ULAW_8kHZ_0
      volume: 0.47
    - address: 10.10.10.11
      port: 4447
      position: 3
      format: ALAW_8kHZ_8
      volume: 0.74
)"};

    REQUIRE(config.rtp_endpoints().size() == 2);
    REQUIRE(config.rtp_endpoints().count(6) == 1);
    REQUIRE(config.rtp_endpoints().count(3) == 1);

    REQUIRE(config.rtp_endpoints()[6].address == boost::asio::ip::address_v4::from_string("10.10.10.10"));
    REQUIRE(config.rtp_endpoints()[6].port == 4444);
    REQUIRE(config.rtp_endpoints()[6].position == 6);
    REQUIRE(config.rtp_endpoints()[6].format == rtp_format::ULAW_8kHZ_0);
    REQUIRE(config.rtp_endpoints()[6].volume == 0.47);

    REQUIRE(config.rtp_endpoints()[3].address == boost::asio::ip::address_v4::from_string("10.10.10.11"));
    REQUIRE(config.rtp_endpoints()[3].port == 4447);
    REQUIRE(config.rtp_endpoints()[3].position == 3);
    REQUIRE(config.rtp_endpoints()[3].format == rtp_format::ALAW_8kHZ_8);
    REQUIRE(config.rtp_endpoints()[3].volume == 0.74);
}

TEST_CASE("rtp incomplete throws exception") {
    std::string config1 = R"(
rtp:
  endpoints:
    - port: 4444
      position: 6
      format: BARIX_PCM_8kHZ_104
)";
    REQUIRE_THROWS_AS(irr_config{config1}, std::exception);

        std::string config2 = R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      position: 6
      format: BARIX_PCM_8kHZ_104
)";
    REQUIRE_THROWS_AS(irr_config{config2}, std::exception);

        std::string config3 = R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      format: BARIX_PCM_8kHZ_104
)";
    REQUIRE_THROWS_AS(irr_config{config3}, std::exception);

        std::string config4 = R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
)";
    REQUIRE_THROWS_AS(irr_config{config4}, std::exception);
}

TEST_CASE("rtp double position") {
    std::string config = R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: BARIX_PCM_8kHZ_104
    - address: 10.10.10.11
      port: 4444
      position: 6
      format: BARIX_PCM_8kHZ_104
)";

    REQUIRE_THROWS_AS(irr_config{config}, std::exception);
}

TEST_CASE("rtp bad address throws") {
    std::string config = R"(
rtp:
  endpoints:
    - address: 192.168.0.256
      port: 4444
      position: 7
      format: BARIX_PCM_8kHZ_104
)";

    REQUIRE_THROWS_AS(irr_config{config}, std::exception);
}

TEST_CASE("rtp unknown format") {
    std::string config = R"(
rtp:
  endpoints:
    - address: 10.10.10.10
      port: 4444
      position: 6
      format: THIS_IS_NOT_A_VALID_RTP_FORMAT
)";

    REQUIRE_THROWS_AS(irr_config{config}, std::exception);
}

TEST_CASE("rtp comfort") {
    irr_config config{R"(
rtp:
  comfort:
    level: 77.7
)"};

    REQUIRE(config.rtp_comfort_level() == 77.7);
}

TEST_CASE("rtp detection level") {
    irr_config config{R"(
rtp:
  comfort:
    detection_level: 100
)"};

    REQUIRE(config.rtp_detection_level() == 100.0);
}

TEST_CASE("rtp always play") {
    irr_config config{R"(
rtp:
  always_play: true
)"};

    REQUIRE(config.rtp_always_play());
}

TEST_CASE("rtp offset") {
    irr_config config{R"(
rtp:
  offset: 3.5
)"};

    REQUIRE(config.rtp_offset() == 3.5);
}

TEST_CASE("rtp position uncertainty") {
    irr_config config{R"(
rtp:
  position_uncertainty: 0.7
)"};

    REQUIRE(config.rtp_position_uncertainty() == 0.7);
}

TEST_CASE("log level") {
    irr_config config{R"(
log:
  level: 4
)"};

    REQUIRE(config.log_level() == 4);
}

TEST_CASE("log dest") {
    irr_config config{R"(
log:
  destination: systemd
)"};

    REQUIRE(config.log_dest() == log_dest::systemd);
}

TEST_CASE("log dest stdout") {
    irr_config config{R"(
log:
  destination: stdout
)"};

    REQUIRE(config.log_dest() == log_dest::Stdout);
}

TEST_CASE("log ptk xml") {
    irr_config config{R"(
log:
  ptk_xml: true
)"};

    REQUIRE(config.log_ptk_xml() == true);
}

TEST_CASE("log missed frames") {
    irr_config config{R"(
log:
  missed_rtp_frames: 47
)"};

    REQUIRE(config.log_missed_frames() == 47);
}

TEST_CASE("opus complexity") {
    irr_config config{R"(
recordings:
  opus_complexity: 1
)"};

    REQUIRE(config.opus_complexity() == 1);
}

TEST_CASE("opus complexity error") {
    std::string config1{R"(
recordings:
  opus_complexity: -1
)"};

    std::string config2{R"(
recordings:
  opus_complexity: 11
)"};

    REQUIRE_THROWS_AS(irr_config{config1}, std::exception);
    REQUIRE_THROWS_AS(irr_config{config2}, std::exception);
}

TEST_CASE("assume_death_seconds") {
    irr_config config{R"(
recordings:
  assume_death_seconds: 47
)"};

    REQUIRE(config.assume_death_seconds() == 47);
}

TEST_CASE("rtp_thread_lock_sec") {
    irr_config config{R"(
log:
  rtp_thread_lock_sec: 0.46
)"};

    REQUIRE(config.rtp_thread_lock_sec() == 0.46);
}

TEST_CASE("rtp_ran_for_sec") {
    irr_config config{R"(
log:
  rtp_ran_for_sec: 0.47
)"};

    REQUIRE(config.rtp_ran_for_sec() == 0.47);
}
