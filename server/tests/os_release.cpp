#include "../os_release.h"

#include <catch.hpp>

TEST_CASE("basic NAME") {
    auto e = parse_os_release("NAME=Unix\nWHAT=gif\n");

    REQUIRE(e["NAME"] == "Unix");
    REQUIRE(e["WHAT"] == "gif");
}

TEST_CASE("last line") {
    auto e = parse_os_release("HELLO=whatever\nNAME=bob");

    REQUIRE(e["HELLO"] == "whatever");
    REQUIRE(e["NAME"] == "bob");
}

TEST_CASE("single quotes") {
    auto e = parse_os_release("NAME='hello bob'\nVALUE=no");

    REQUIRE(e["NAME"] == "hello bob");
    REQUIRE(e["VALUE"] == "no");
}

TEST_CASE("double quotes") {
    auto e = parse_os_release("HELLO=\"Hello there\"");

    REQUIRE(e["HELLO"] == "Hello there");
}

// some real os-release files stolen from various distributions

TEST_CASE("os-release debian stretch") {
    auto e = parse_os_release(R"os-release(PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
)os-release");

    REQUIRE(e["PRETTY_NAME"] == "Debian GNU/Linux 9 (stretch)");
    REQUIRE(e["NAME"] == "Debian GNU/Linux");
    REQUIRE(e["VERSION_ID"] == "9");
    REQUIRE(e["VERSION"] == "9 (stretch)");
    REQUIRE(e["ID"] == "debian");
    REQUIRE(e["HOME_URL"] == "https://www.debian.org/");
    REQUIRE(e["SUPPORT_URL"] == "https://www.debian.org/support");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.debian.org/");
}

TEST_CASE("os-release debian jessie") {
    auto e = parse_os_release(R"os-release(PRETTY_NAME="Debian GNU/Linux 8 (jessie)"
NAME="Debian GNU/Linux"
VERSION_ID="8"
VERSION="8 (jessie)"
ID=debian
HOME_URL="http://www.debian.org/"
SUPPORT_URL="http://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
)os-release");

    REQUIRE(e["PRETTY_NAME"] == "Debian GNU/Linux 8 (jessie)");
    REQUIRE(e["NAME"] == "Debian GNU/Linux");
    REQUIRE(e["VERSION_ID"] == "8");
    REQUIRE(e["VERSION"] == "8 (jessie)");
    REQUIRE(e["ID"] == "debian");
    REQUIRE(e["HOME_URL"] == "http://www.debian.org/");
    REQUIRE(e["SUPPORT_URL"] == "http://www.debian.org/support");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.debian.org/");
}

TEST_CASE("os-release debian buster/sid") {
    auto e = parse_os_release(R"os-release(PRETTY_NAME="Debian GNU/Linux buster/sid"
NAME="Debian GNU/Linux"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
)os-release");

    REQUIRE(e["PRETTY_NAME"] == "Debian GNU/Linux buster/sid");
    REQUIRE(e["NAME"] == "Debian GNU/Linux");
    REQUIRE(e["ID"] == "debian");
    REQUIRE(e["HOME_URL"] == "https://www.debian.org/");
    REQUIRE(e["SUPPORT_URL"] == "https://www.debian.org/support");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.debian.org/");
}

TEST_CASE("os-release arch linux") {
    auto e = parse_os_release(R"os-release(NAME="Arch Linux"
PRETTY_NAME="Arch Linux"
ID=arch
BUILD_ID=rolling
ANSI_COLOR="0;36"
HOME_URL="https://www.archlinux.org/"
DOCUMENTATION_URL="https://wiki.archlinux.org/"
SUPPORT_URL="https://bbs.archlinux.org/"
BUG_REPORT_URL="https://bugs.archlinux.org/"
)os-release");

    REQUIRE(e["NAME"] == "Arch Linux");
    REQUIRE(e["PRETTY_NAME"] == "Arch Linux");
    REQUIRE(e["ID"] == "arch");
    REQUIRE(e["BUILD_ID"] == "rolling");
    REQUIRE(e["ANSI_COLOR"] == "0;36");
    REQUIRE(e["HOME_URL"] == "https://www.archlinux.org/");
    REQUIRE(e["DOCUMENTATION_URL"] == "https://wiki.archlinux.org/");
    REQUIRE(e["SUPPORT_URL"] == "https://bbs.archlinux.org/");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.archlinux.org/");
}

TEST_CASE("os-release ubuntu cosmic") {
    auto e = parse_os_release(R"os-release(NAME="Ubuntu"
VERSION="18.10 (Cosmic Cuttlefish)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 18.10"
VERSION_ID="18.10"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=cosmic
UBUNTU_CODENAME=cosmic
)os-release");

    REQUIRE(e["NAME"] == "Ubuntu");
    REQUIRE(e["VERSION"] == "18.10 (Cosmic Cuttlefish)");
    REQUIRE(e["ID"] == "ubuntu");
    REQUIRE(e["ID_LIKE"] == "debian");
    REQUIRE(e["PRETTY_NAME"] == "Ubuntu 18.10");
    REQUIRE(e["VERSION_ID"] == "18.10");
    REQUIRE(e["HOME_URL"] == "https://www.ubuntu.com/");
    REQUIRE(e["SUPPORT_URL"] == "https://help.ubuntu.com/");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.launchpad.net/ubuntu/");
    REQUIRE(e["PRIVACY_POLICY_URL"] == "https://www.ubuntu.com/legal/terms-and-policies/privacy-policy");
    REQUIRE(e["VERSION_CODENAME"] == "cosmic");
    REQUIRE(e["UBUNTU_CODENAME"] == "cosmic");
}

TEST_CASE("os-release ubuntu bionic") {
    auto e = parse_os_release(R"os-release(NAME="Ubuntu"
VERSION="18.04.1 LTS (Bionic Beaver)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 18.04.1 LTS"
VERSION_ID="18.04"
HOME_URL="https://www.ubuntu.com/"
SUPPORT_URL="https://help.ubuntu.com/"
BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
VERSION_CODENAME=bionic
UBUNTU_CODENAME=bionic
)os-release");

    REQUIRE(e["NAME"] == "Ubuntu");
    REQUIRE(e["VERSION"] == "18.04.1 LTS (Bionic Beaver)");
    REQUIRE(e["ID"] == "ubuntu");
    REQUIRE(e["ID_LIKE"] == "debian");
    REQUIRE(e["PRETTY_NAME"] == "Ubuntu 18.04.1 LTS");
    REQUIRE(e["VERSION_ID"] == "18.04");
    REQUIRE(e["HOME_URL"] == "https://www.ubuntu.com/");
    REQUIRE(e["SUPPORT_URL"] == "https://help.ubuntu.com/");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugs.launchpad.net/ubuntu/");
    REQUIRE(e["PRIVACY_POLICY_URL"] == "https://www.ubuntu.com/legal/terms-and-policies/privacy-policy");
    REQUIRE(e["VERSION_CODENAME"] == "bionic");
    REQUIRE(e["UBUNTU_CODENAME"] == "bionic");
}

// stolen from os-release man page
TEST_CASE("os-release fedora") {
    auto e = parse_os_release(R"os-release(NAME=Fedora
VERSION="17 (Beefy Miracle)"
ID=fedora
VERSION_ID=17
PRETTY_NAME="Fedora 17 (Beefy Miracle)"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:fedoraproject:fedora:17"
HOME_URL="https://fedoraproject.org/"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
)os-release");

    REQUIRE(e["NAME"] == "Fedora");
    REQUIRE(e["VERSION"] == "17 (Beefy Miracle)");
    REQUIRE(e["ID"] == "fedora");
    REQUIRE(e["VERSION_ID"] == "17");
    REQUIRE(e["PRETTY_NAME"] == "Fedora 17 (Beefy Miracle)");
    REQUIRE(e["ANSI_COLOR"] == "0;34");
    REQUIRE(e["CPE_NAME"] == "cpe:/o:fedoraproject:fedora:17");
    REQUIRE(e["HOME_URL"] == "https://fedoraproject.org/");
    REQUIRE(e["BUG_REPORT_URL"] == "https://bugzilla.redhat.com/");
}
