#include <catch.hpp>

#include "../ptk_client.h"

#pragma clang diagnostic ignored "-Wfloat-equal"

TEST_CASE("parse_position_string null") {
    REQUIRE(parse_position_string("").size() == 0);
}

TEST_CASE("parse_position_string one") {
    auto set = parse_position_string("P3");
    REQUIRE(set.size() == 1);
    REQUIRE(set.count(3));
}

TEST_CASE("parse_position_string other") {
    auto set = parse_position_string("\tP2\tC2\t");
    REQUIRE(set.size() == 1);
    REQUIRE(set.count(2));
}

TEST_CASE("parse_position_string no whitespace") {
    auto set = parse_position_string("P2\t\tP3");
    REQUIRE(set.size() == 2);
    REQUIRE(set.count(2));
    REQUIRE(set.count(3));
}

TEST_CASE("parse_position_string crazy whitespace") {
    auto set = parse_position_string("  \t \t\t P2\tP3   P4");
    REQUIRE(set.size() == 3);
    REQUIRE(set.count(2));
    REQUIRE(set.count(3));
    REQUIRE(set.count(4));
}

TEST_CASE("parse_position_string different type") {
    auto set = parse_position_string("\tP1\t\tX3\t", 'X');
    REQUIRE(set.size() == 1);
    REQUIRE(set.count(3));
    REQUIRE(!set.count(1));
}

TEST_CASE("parse date string") {
    REQUIRE(parse_date_string("2011/07/23 17:13:03.000 UTC") == 1311441183);

    // leap second
    REQUIRE(parse_date_string("2017/01/01 00:00:00.000 UTC") == 1483228800);
    REQUIRE(parse_date_string("2016/12/31 23:59:60.000 UTC") == 1483228800);
    REQUIRE(parse_date_string("2016/12/31 23:59:59.000 UTC") == 1483228799);

    // fractional
    REQUIRE(parse_date_string("1997/01/28 05:12:07.888 UTC") == 854428327.888);
}
