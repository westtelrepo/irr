#include <catch.hpp>

#include "../rtp.h"

TEST_CASE("positions_at_time null") {
    std::map<double, std::set<int>> positions;
    REQUIRE(positions_at_time(positions, 0).empty());

    REQUIRE(positions.empty());
}

TEST_CASE("positions_at_time before") {
    std::map<double,std::set<int>> positions;
    positions[3.5] = std::set<int>{1};

    REQUIRE(positions_at_time(positions, 3.2).empty());

    REQUIRE(positions.size() == 1);
}

TEST_CASE("positions_at_time after") {
    std::map<double,std::set<int>> positions;
    positions[3.5] = std::set<int>{1};

    auto pos = positions_at_time(positions, 3.7);

    REQUIRE(pos.size() == 1);
    REQUIRE(pos.count(1));

    REQUIRE(positions.size() == 1);
}

TEST_CASE("positions_at_time many") {
    std::map<double, std::set<int>> positions;
    positions[1] = std::set<int>{1};
    positions[2] = std::set<int>{2,3};
    positions[3] = std::set<int>{4,5,6};

    auto pos = positions_at_time(positions, 2.3);

    REQUIRE(pos.size() == 2);
    REQUIRE(pos.count(2));
    REQUIRE(pos.count(3));

    REQUIRE(positions.size() == 2);
    REQUIRE(positions.count(2));
    REQUIRE(positions.count(3));
}

TEST_CASE("positions_at_time offset before") {
    std::map<double, std::set<int>> positions;
    positions[3.5] = std::set<int>{1};

    auto pos = positions_at_time(positions, 3.4, 0.5);

    REQUIRE(pos.size() == 1);
    REQUIRE(pos.count(1));

    REQUIRE(positions.size() == 1);
}

TEST_CASE("positions_at_time offset after") {
    std::map<double, std::set<int>> positions;
    positions[3.5] = std::set<int>{1};
    positions[4] = std::set<int>{2,3};

    auto pos = positions_at_time(positions, 4.1, 1);

    REQUIRE(pos.size() == 3);
    REQUIRE(pos.count(1));
    REQUIRE(pos.count(2));
    REQUIRE(pos.count(3));

    REQUIRE(positions.size() == 2);
}
