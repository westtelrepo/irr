#include "wav_reader.h"

#include <regex>
#include <systemd/sd-daemon.h>

#include "config.h"
#include "globals.h"
#include "logger.h"

static_assert(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__, "Little endian only");

struct riff_header {
	char riff[4];
	uint32_t size;
	char wave[4];
};
static_assert(offsetof(riff_header, riff) == 0, "riff_header");
static_assert(offsetof(riff_header, size) == 4, "riff_header");
static_assert(offsetof(riff_header, wave) == 8, "riff_header");
static_assert(sizeof(riff_header) == 12, "riff_header");

struct chunk_header {
	char type[4];
	uint32_t size;
};

// fmt subchunk
struct fmt_chunk {
	uint16_t audio_format; // 1
	uint16_t channels; // 1 or 2
	uint32_t sample_rate; // 16000
	uint32_t byte_rate;
	uint16_t block_align;
	uint16_t bits_per_sample; // 16
};

enum class parse_state {
	riff_header,
	chunk_header,
	unknown_chunk,
	fmt_chunk,
	data_chunk,
};

static void for_each_channel(int channels, const std::vector<char>& data, std::function<void(int,std::vector<int16_t>)> f) {
	// data's size must have an even number of samples per channel
	assert(data.size() % (sizeof(int16_t) * static_cast<size_t>(channels)) == 0);

	for (int channel = 0; channel < channels; ++channel) {
		std::vector<int16_t> pcm(data.size() / (sizeof(int16_t) * static_cast<size_t>(channels)));
		for (size_t sample = 0; sample < pcm.size(); ++sample) {
			std::memcpy(pcm.data() + sample,
				data.data() + static_cast<size_t>(channels) * sample * sizeof(int16_t) + static_cast<size_t>(channel) * sizeof(int16_t),
				sizeof(int16_t));
		}
		f(channel, std::move(pcm));
	}
}

class wavReader::wav {
public:
	posixFd fd;
	std::queue<char> buf;
	bool done = false;

	// if error, can only be removed by either removing/deleting it
	// or restarting IRR with a new version to fix whatever
	// is going on
	bool error = false;

	parse_state state = parse_state::riff_header;
	size_t chunk_size;

	opus::Encoder left;
	opus::Encoder right;

	uint64_t piece = 0;

	int channels;
	int32_t sampling_rate;

	// we use steady_clock because we don't care what time it is, we only care about differences in time
	std::chrono::steady_clock::time_point last_update = std::chrono::steady_clock::now();

	std::string streamid; // without the :L or :R
	std::string callid;
	double unix_time;

	wav() = delete;
	wav(posixFd&& f): fd{std::move(f)} { }

	wav(const wav&) = delete;
	wav(wav&&) = default;

	wav& operator=(const wav&) = delete;
	wav& operator=(wav&&) = default;

	template<class T>
	T pop_struct() {
		assert(buf.size() >= sizeof(T));
		std::array<char, sizeof(T)> data;
		for (size_t i = 0; i < sizeof(T); ++i) {
			data[i] = buf.front();
			buf.pop();
		}
		T ret;
		std::memcpy(&ret, data.data(), sizeof(T));
		return ret;
	}

	std::vector<char> pop_vector(size_t size) {
		assert(buf.size() >= size);
		std::vector<char> ret;
		ret.reserve(size);
		for (size_t i = 0; i < size; ++i) {
			ret.push_back(buf.front());
			buf.pop();
		}
		return ret;
	}
};

struct wavReader::impl {
	bool read_files_running = false;

	std::map<boost::filesystem::path, wav> files;

	boost::asio::steady_timer lsTimer;
	boost::asio::steady_timer readTimer;

	std::function<void(std::string,uint64_t,std::vector<char>)> send_opus;

	// notification that a "stream" has ended.
	// uint16_t parameter is length of last piece (1..960)
	// function is when database has been written
	std::function<void(std::string,uint16_t,std::function<void()>)> end_opus;

	// streamid, unix timestamp, callid
	std::function<void(std::string,double,std::string)> set_info;

	impl(boost::asio::io_service& io,
		std::function<void(std::string,uint64_t,std::vector<char>)> send,
		std::function<void(std::string,uint16_t,std::function<void()>)> end,
		std::function<void(std::string,double,std::string)> info)
	: lsTimer{io}, readTimer{io}, send_opus{send}, end_opus{end}, set_info{info}
	{ }

	bool all_done() const {
		bool ret = true;
		for (const auto& file: files)
			ret = ret && file.second.done;
		return ret;
	}

	std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)> config_update;
};

wavReader::wavReader(boost::asio::io_service& io,
	std::function<void(std::string,uint64_t,std::vector<char>)> send_opus,
	std::function<void(std::string,uint16_t,std::function<void()>)> end_opus,
	std::function<void(std::string,double,std::string)> info)
	: impl_{ new impl{io, send_opus, end_opus, info}, [](impl* i){ delete i;} }
{ }

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static const std::regex v1_filename{R"(v1_(\d*\.?\d*)_([^_]+)_([^.]+)\.wav)"};
#pragma clang diagnostic pop

void wavReader::start() {
	impl_->config_update = [this] (std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config> new_config) {
		for (auto& file: impl_->files) {
			if (file.second.left)
				file.second.left.complexity(new_config->opus_complexity());
			if (file.second.right)
				file.second.right.complexity(new_config->opus_complexity());
		}
	};

	config_subscribe(
        std::shared_ptr<
            std::function<void(std::shared_ptr<const irr_config>, std::shared_ptr<const irr_config>)>
        >{shared_from_this(), &impl_->config_update}
    );

	impl_->lsTimer.expires_from_now(std::chrono::milliseconds(1));
	impl_->lsTimer.async_wait(std::bind(&wavReader::check_new_files, this->shared_from_this()));
}

void wavReader::check_new_files() {
	try {
		auto config = config_get();
		boost::filesystem::path dir{config->recording_dir()};
		if (boost::filesystem::is_directory(dir)) {
			for (auto i = boost::filesystem::directory_iterator{dir};
			i != boost::filesystem::directory_iterator{}; ++i) {
				// if wrong extension we skip
				const auto& str = i->path().string();
				if (str.size() < 4 || str.substr(str.size() - 4) != ".wav")
					continue;

				// if not already added, add
				if (!impl_->files.count(i->path())) {
					IRR_DEBUG("Found new file " << i->path());
					impl_->files.emplace(std::make_pair(i->path(), wav{posixFd::open(i->path().c_str(), O_RDONLY)}));
					auto& wav = impl_->files.find(i->path())->second;

					std::smatch smatch;
					std::string filename = i->path().filename().string();
					if (std::regex_match(filename, smatch, v1_filename)) {
						wav.streamid = smatch[3].str();
						wav.callid = smatch[2].str();
						wav.unix_time = std::stod(smatch[1].str());
					} else {
						wav.streamid = i->path().filename().string();
						wav.callid = wav.streamid;
						// NOTE: std::time is guaranteed to return UNIX epoch on Linux and most other systems,
						// but is not defined as such by the C standard
						wav.unix_time = std::time(nullptr);
					}
				}
			}
			global_irr_status->clear("wav:ls");
		} else {
			global_irr_status->set_alarm("wav:ls", alarm_priority::error, "Recording directory doesn't exist", dir.c_str());
		}
	} catch(const boost::filesystem::filesystem_error& e) {
		global_irr_status->set_alarm("wav:ls", alarm_priority::error, "Can not look for new files", e.what());
	} catch(const std::exception& e) {
		global_irr_status->set_alarm("wav:ls", alarm_priority::programming_error, "Can not look for new files", e.what());
	}

	for (auto i = impl_->files.begin(); i != impl_->files.end();) {
		if (i->second.done && i->second.error && !boost::filesystem::exists(i->first)) {
			// file has been removed by admin intervention
			global_irr_status->clear("wav:read", i->first.string());
			i = impl_->files.erase(i);
		} else
			++i;
	}

	impl_->lsTimer.expires_from_now(std::chrono::milliseconds(500));
	impl_->lsTimer.async_wait(std::bind(&wavReader::check_new_files, this->shared_from_this()));

	if (!impl_->read_files_running && !impl_->all_done()) {
		impl_->read_files_running = true;
		impl_->readTimer.expires_from_now(std::chrono::milliseconds(10));
		impl_->readTimer.async_wait(std::bind(&wavReader::read_files, this->shared_from_this()));
	}

	// if there are no files in the array that means that there are no files on disk
	// and that all data has been pushed to the database
	if (global_graceful_shutdown == 1 && impl_->files.size() == 0) {
		IRR_NOTICE("<5>Shutting down, all file data flushed to DB");
		std::_Exit(0);
	} else if (global_graceful_shutdown == 2) {
		// if every file is "done" that means that either
		// 1) there are no files
		// 2) one or more files has not had data flushed to DB
		// 3) one or more files has an error of some sort (like bad format) and
		//    it's keeping the file in the array to keep track of it
		if (impl_->all_done()) {
			IRR_NOTICE("Shutting down, no current files in recording directory");
			std::_Exit(0);
		}
	}
}

void wavReader::read_files() {
	auto config = config_get();

	// instead of iterating impl_->files directly, we copy it into an array and then sort it by the unix date
	// of the file. This allows us to prioritize the most recent files for encoding and then encode older files
	// on an as-possible basis.
	std::vector<std::reference_wrapper<std::pair<const boost::filesystem::path, wavReader::wav>>> file_list;
	file_list.reserve(impl_->files.size());

	for (auto& f: impl_->files)
		file_list.push_back(std::ref(f));

	// if you have enough files that sorting it becomes an issue, you have too many files
	std::stable_sort(file_list.begin(), file_list.end(),
	[] (std::pair<const boost::filesystem::path, wavReader::wav>& x,
		std::pair<const boost::filesystem::path, wavReader::wav>& y) {
		return x.second.unix_time > y.second.unix_time;
	});

	auto start_time = std::chrono::steady_clock::now();

	for (size_t file_index = 0; file_index < file_list.size(); file_index++) {
		auto& f = file_list[file_index].get();

		try {
			// this flag is because we delete asynchronously, so we will ignore it instead
			// of re-adding it
			if (f.second.done)
				continue;

			// 512 ms for 48kHZ 2 channel for each read
			std::array<char, 6 * 16384> arr;
			size_t count = 0;
			if (f.second.buf.size() <= 18 * 16384)
				count = f.second.fd.read(arr);

			// if we read something, update the time
			if (count != 0)
				f.second.last_update = std::chrono::steady_clock::now();

			for (size_t i = 0; i < count; ++i)
				f.second.buf.push(arr[i]);

			// if the buffer is less than this we can consider this a possible EOF
			size_t next_read;

			switch (f.second.state) {
				case parse_state::riff_header:
					next_read = sizeof(riff_header);
					if (f.second.buf.size() >= sizeof(riff_header)) {
						riff_header header = f.second.pop_struct<riff_header>();
						if (memcmp(header.riff, "RIFF", 4) || memcmp(header.wave, "WAVE", 4)) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Invalid RIFF header", f.first.string());
							continue;
						}
						f.second.state = parse_state::chunk_header;
					}
					break;
				case parse_state::chunk_header:
					next_read = sizeof(chunk_header);
					if (f.second.buf.size() >= sizeof(chunk_header)) {
						chunk_header header = f.second.pop_struct<chunk_header>();
						if (!memcmp(header.type, "fmt ", 4)) {
							f.second.state = parse_state::fmt_chunk;
							f.second.chunk_size = header.size;
						} else if (!memcmp(header.type, "data", 4)) {
							f.second.state = parse_state::data_chunk;
						} else {
							f.second.state = parse_state::unknown_chunk;
							f.second.chunk_size = header.size;
						}
					}
					break;
				case parse_state::unknown_chunk:
					next_read = f.second.chunk_size;
					if (f.second.buf.size() >= f.second.chunk_size) {
						for (size_t i = 0; i < f.second.chunk_size; ++i)
							f.second.buf.pop();
						f.second.state = parse_state::chunk_header;
					}
					break;
				case parse_state::fmt_chunk:
					next_read = f.second.chunk_size;
					if (f.second.buf.size() >= f.second.chunk_size) {
						const std::vector<char> data = f.second.pop_vector(f.second.chunk_size);
						if (data.size() < sizeof(fmt_chunk)) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Invalid fmt chunk", f.first.string());
							continue;
						}
						fmt_chunk chunk;
						std::memcpy(&chunk, data.data(), sizeof(fmt_chunk));
						if (chunk.audio_format != 1) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Invalid RIFF header", f.first.string());
							continue;
						}

						if (chunk.channels != 1 && chunk.channels != 2) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Unsupported number of channels", f.first.string());
							continue;
						}
						f.second.channels = chunk.channels;

						if (chunk.sample_rate != 8000 && chunk.sample_rate != 12000 && chunk.sample_rate != 16000 && chunk.sample_rate != 24000 && chunk.sample_rate != 48000) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Unsupported sample rate", f.first.string());
							continue;
						}
						f.second.sampling_rate = static_cast<int32_t>(chunk.sample_rate);

						if (chunk.bits_per_sample != 16) {
							f.second.done = true;
							f.second.error = true;
							global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Unsupported bits-per-sample", f.first.string());
							continue;
						}
						f.second.state = parse_state::chunk_header;

						f.second.left = opus::Encoder{f.second.sampling_rate, 1, OPUS_APPLICATION_AUDIO};
						f.second.left.complexity(config->opus_complexity());
						if (f.second.channels == 2) {
							f.second.right = opus::Encoder{f.second.sampling_rate, 1, OPUS_APPLICATION_AUDIO};
							f.second.right.complexity(config->opus_complexity());
						}

						if (f.second.channels == 2) {
							impl_->set_info(f.second.streamid + ":L", f.second.unix_time, f.second.callid);
							impl_->set_info(f.second.streamid + ":R", f.second.unix_time, f.second.callid);
						} else
							impl_->set_info(f.second.streamid, f.second.unix_time, f.second.callid);
					}
					break;
				case parse_state::data_chunk:
					next_read = static_cast<size_t>(f.second.sampling_rate / 50 * 2 * f.second.channels);
					// only do so many pieces at a time
					// 25 pieces is 500 ms
					for (size_t cnti = 0; cnti < 25; ++cnti) {
						if (f.second.buf.size() >= next_read) {
							for_each_channel(f.second.channels, f.second.pop_vector(next_read), [&] (int channel, std::vector<int16_t> pcm) {
								assert(channel == 0 || channel == 1);
								std::array<char, 4096> opus;
								const size_t opus_len = (channel == 0 ? f.second.left : f.second.right).encode(pcm.data(), static_cast<int>(pcm.size()), opus.data(), opus.size());

								std::vector<char> send(opus_len);
								std::copy(opus.data(), opus.data() + opus_len, send.data());

								std::string streamid = f.second.streamid;
								if (f.second.channels == 2)
									streamid += channel == 0 ? ":L" : ":R";
								impl_->send_opus(streamid, f.second.piece, std::move(send));
							});
							f.second.piece++;
						} else
							break;
					}
					break;
			}

			// EOF. Let's check if FS is truly done
			if (count == 0 && f.second.buf.size() < next_read) {
				bool file_size_is_correct;
				{
					saveSeek save(f.second.fd);
					f.second.fd.lseek(0, SEEK_SET);

					riff_header riff;
					size_t rcount = f.second.fd.read(&riff, sizeof(riff_header));
					off_t file_size = f.second.fd.lseek(0, SEEK_END);

					if (rcount != sizeof riff)
						file_size_is_correct = false;
					else if (static_cast<off_t>(riff.size) == file_size - 8)
						file_size_is_correct = true;
					else
						file_size_is_correct = false;
				}

				std::chrono::seconds time_since = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - f.second.last_update);

				// if file hasn't changed for some time, assume freeswitch died
				if (file_size_is_correct || time_since.count() > config->assume_death_seconds()) {
					f.second.done = true;
					IRR_INFO(f.first << " is done");

					const size_t left_bytes = f.second.state == parse_state::data_chunk ? f.second.buf.size() : 0;
					// send rest of PCM data
					if (f.second.state == parse_state::data_chunk && left_bytes > 0) {
						std::vector<char> data(static_cast<size_t>(f.second.sampling_rate) / 50 * 2 * static_cast<size_t>(f.second.channels));
						// initialize array with 0, i.e. silence
						std::fill(data.begin(), data.end(), 0);

						for (size_t i = 0; i < data.size(); ++i) {
							if (f.second.buf.size() == 0)
								break;
							data[i] = f.second.buf.front();
							f.second.buf.pop();
						}

						for_each_channel(f.second.channels, data, [&] (int channel, std::vector<int16_t> pcm) {
							assert(channel == 0 || channel == 1);
							std::array<char, 4096> opus;
							const size_t opus_len = (channel == 0 ? f.second.left : f.second.right).encode(pcm.data(), static_cast<int>(pcm.size()), opus.data(), opus.size());

							std::vector<char> send(opus_len);
							std::copy(opus.data(), opus.data() + opus_len, send.data());

							std::string streamid = f.second.streamid;
							if (f.second.channels == 2)
								streamid += channel == 0 ? ":L" : ":R";
							impl_->send_opus(streamid, f.second.piece, std::move(send));
						});
					}

					size_t end_size;
					if (f.second.state == parse_state::data_chunk)
						end_size = left_bytes == 0 ? 960 : 24000 * left_bytes / static_cast<size_t>(f.second.sampling_rate * f.second.channels);
					else
						end_size = 0;

					auto times = std::make_shared<int>(0);
					int channels = f.second.channels;
					boost::filesystem::path filename = f.first;

					// delete file only when both streams have finished
					auto remove = [this, self = shared_from_this(), times, filename, channels] () {
						(*times)++;
						if (*times == channels) {
							int err = unlink(filename.c_str());
							if (err == -1) {
								perror("Could not unlink file");
								global_irr_status->set_alarm("wav:read", filename.string(), alarm_priority::other_error, "Could not delete file", filename.string());
								impl_->files.at(filename).error = true;
								// done is already set
							} else {
								// having this erase here is fine, it will never run inside of the impl_->files for loop
								// because this io_service is single-threaded
								impl_->files.erase(filename);
								global_irr_status->clear("wav:read", filename.string());
							}

						}
					};

					impl_->end_opus(f.second.streamid + (f.second.channels == 2 ? ":L" : ""), static_cast<uint16_t>(end_size), remove);
					if (f.second.channels == 2)
						impl_->end_opus(f.second.streamid + ":R", static_cast<uint16_t>(end_size), remove);
				}
			}
		} catch(const std::exception& e) {
			f.second.done = true;
			f.second.error = true;
			global_irr_status->set_alarm("wav:read", f.first.string(), alarm_priority::other_error, "Exception reading wav file", f.first.string() + ": " + e.what());
		}

		// we break if more than 100ms have passed since we started encoding this pass
		// that means files will be ignored, for a time. However we will do them again
		// when we come back around.
		if (std::chrono::steady_clock::now() - start_time > std::chrono::milliseconds(100)) {
			break;
		}
	}

	if (!impl_->all_done()) {
		impl_->readTimer.expires_from_now(std::chrono::milliseconds(10));
		impl_->readTimer.async_wait(std::bind(&wavReader::read_files, this->shared_from_this()));
	} else
		impl_->read_files_running = false;
}

