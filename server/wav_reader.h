#ifndef WESTTEL_WAV_READER
#define WESTTEL_WAV_READER

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/chrono.hpp>
#include <boost/filesystem.hpp>
#include <functional>
#include <map>
#include <memory>
#include <queue>

#include "../common/posixFd.h"
#include "../common/pcm.h"
#include "../common/opus.hpp"

class wavReader: public std::enable_shared_from_this<wavReader> {
private:
	class wav;
	struct impl;
	std::unique_ptr<impl, void(*)(impl*)> impl_;

	wavReader(boost::asio::io_service& io,
		std::function<void(std::string,uint64_t,std::vector<char>)> send_opus,
		std::function<void(std::string,uint16_t,std::function<void()>)> end_opus,
		std::function<void(std::string,double,std::string)> info);

	void check_new_files();
	void read_files();
public:
	// this uses "variadic templates" and "perfect forwarding" so all "private" constructors
	// can be called from this factory method without lots of boilerplate, and they will
	// all be wrapped in a std::shared_ptr
	template<class... Ts>
	static std::shared_ptr<wavReader> create(Ts&&... ts) {
		return std::shared_ptr<wavReader>{ new wavReader{std::forward<Ts>(ts)...} };
	}

	void start();
};

#endif
