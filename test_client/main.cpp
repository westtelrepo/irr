#include "../common/tcp_frame.h"

#include <boost/asio.hpp>
#include <boost/asio/steady_timer.hpp>
#include <cassert>
#include <iostream>
#include <memory>
#include <mutex>
#include <pulse/simple.h>
#include <queue>
#include <thread>
#include <json/json.h>

#include "../common/opus.hpp"
using boost::asio::ip::tcp;

static_assert(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__, "Little endian only");

//static std::queue<std::array<int16_t, 960>> queue;

// stream id -> piece num -> data
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wexit-time-destructors"
#pragma clang diagnostic ignored "-Wglobal-constructors"
static std::map<std::string,std::map<uint64_t,std::vector<char>>> opus_pieces;
static std::string current_stream = "";
static opus::Decoder decode{48000, 1};
#pragma clang diagnostic pop

static uint64_t current_pos = 0;
static std::mutex mutex;

class irr_client {
private:
	tcp_frames socket_;
	std::queue<std::string> write_;
	std::string writebuf_;

	std::map<int64_t, std::function<void(Json::Value)>> callbacks_;
	int64_t r_ = 1;

	void read() {
		socket_.async_read([this](const boost::system::error_code& e, std::vector<char> data) {
			if (e || data.size() < 1)
				return;

			switch(data.at(0)) {
				case 1: {
					std::stringstream ss{std::string{data.data() + 1, data.size() - 1}};
					uint64_t pnum = 0;
					ss.read(reinterpret_cast<char*>(&pnum), sizeof pnum);

					std::string stream;
					std::getline(ss, stream, '\0');

					std::vector<char> buf;
					buf.resize(data.size() - 1 - sizeof pnum);
					ss.read(buf.data(), static_cast<std::streamsize>(buf.size()));
					buf.resize(static_cast<size_t>(ss.gcount()));
					buf.shrink_to_fit();

					//std::cout << "PIECE " << stream << " " << pnum << " " << buf.size() << std::endl;
					{
						std::lock_guard<std::mutex> lock_guard{mutex};
						opus_pieces[stream][pnum] = std::move(buf);
					}
					break;
				}
				case 2: {
					std::stringstream ss{std::string{data.data() + 1, data.size() - 1}};
					Json::Value root;
					ss >> root;
					std::cout << root << std::endl;

					int64_t r = 0;
					if (root.isMember("_r"))
						r = root["_r"].asInt64();

					if (r < 0) {
						auto f = callbacks_.find(-r);
						if (f != callbacks_.end()) {
							f->second(std::move(root));
							callbacks_.erase(f);
						}
					} else {
						if (!root.isMember("_c"))
							return;
						std::string command = root["_c"].asString();
						if (command == "PING" && r > 0) {
							Json::Value ret;
							ret["_r"] = Json::Value{static_cast<Json::Int64>(-r)};
							ret["_c"] = "PONG";
							send(ret);
						}
					}
					break;
				}
				default:
					std::cout << "BROKEN" << std::endl;
					return;
			}

			read();
			return;
		});
	}

	void write() {
		writebuf_ = write_.front();
		write_.pop();

		socket_.async_write(writebuf_.data(), writebuf_.size(),
		[this] (const boost::system::error_code& e) {
			if (e) return;
			if (write_.size())
				write();
		});
	}
public:
	irr_client(tcp::socket&& s): socket_{std::move(s)} { read(); }

	void send(std::string command) {
		write_.push(command);
		write();
	}

	void send(const Json::Value& v) {
		Json::FastWriter w;
		send("\2" + w.write(v));
	}

	void send(Json::Value v, std::function<void(Json::Value)> f) {
		callbacks_[r_] = std::move(f);
		v["_r"] = Json::Value{static_cast<Json::Int64>(r_++)};
		send(v);
	}

	void ping(std::function<void()> f) {
		Json::Value p;
		p["_c"] = "PING";
		send(p, [f] (Json::Value) { f(); });
	}

	void subscribe(size_t pos) {
		Json::Value s;
		s["_c"] = "SUBSCRIBE";
		s["pos"] = static_cast<Json::Int64>(pos);
		send(s);
	}

	void list() {
		Json::Value list;
		list["_c"] = "LIST";
		send(list, [] (Json::Value) {});
	}

	void get(std::string stream, size_t p1, size_t p2) {
		Json::Value get;
		get["_c"] = "GET";
		get["stream"] = stream;
		get["first"] = static_cast<Json::UInt64>(p1);
		get["last"] = static_cast<Json::UInt64>(p2);
		send(get);
	}

	void play(std::string stream) {
		Json::Value play;
		play["_c"] = "PLAY";
		play["stream"] = std::move(stream);
		send(std::move(play));
	}

	void stop(std::string stream) {
		Json::Value stop;
		stop["_c"] = "STOP";
		stop["stream"] = std::move(stream);
		send(std::move(stop));
	}

	void listplay() {
		Json::Value list;
		list["_c"] = "LISTPLAY";
		send(std::move(list), [] (Json::Value) {});
	}

	void listcurrent() {
		Json::Value list;
		list["_c"] = "LISTCURRENT";
		send(std::move(list), [] (Json::Value) {});
	}

	void info(std::string stream) {
		Json::Value info;
		info["_c"] = "STREAMINFO";
		info["stream"] = stream;
		send(info, [] (Json::Value) {});
	}
};
static irr_client* glirr = nullptr;

class tui {
private:
	boost::asio::posix::stream_descriptor in_;
	boost::asio::streambuf inbuf_;
	boost::asio::steady_timer timer_;
public:
	tui(boost::asio::io_service& io_service): in_{io_service, dup(STDIN_FILENO)},
	timer_{io_service} { read(); }

private:
	void read() {
		boost::asio::async_read_until(in_, inbuf_, '\n', [this](const boost::system::error_code& e,size_t) {
			if (e) return;
			std::istream i{&inbuf_};
			std::string line;
			std::getline(i, line);

			std::stringstream ss{line};
			std::string word;
			ss >> word;

			std::lock_guard<std::mutex> lock_guard{mutex};
			// play stream_id start_piece
			// start playing through speaker
			if (word == "play") {
				std::string stream;
				ssize_t pos;
				ss >> stream >> pos;
				current_stream = stream;
				if (pos == -1) {
					auto e2 = opus_pieces[stream].end();
					--e2;
					current_pos = e2->first;
				} else
					current_pos = static_cast<size_t>(pos);
			// list what client has
			} else if (word == "list") {
				std::cout << "list: " << std::endl;
				for (auto&& i2: opus_pieces) {
					auto e2 = i2.second.end();
					--e2;
					std::cout << i2.first << " " << e2->first << std::endl;
				}
			// stop playing
			} else if (word == "stop") {
				current_stream = "";
				current_pos = 0;
			// infamous ping
			} else if (word == "p") {
				glirr->ping([] () { });
			// list what server has
			} else if (word == "slist") {
				glirr->list();
			// get something from the server
			} else if (word == "get") {
				std::string stream;
				size_t p1, p2;
				ss >> stream >> p1 >> p2;
				glirr->get(stream, p1, p2);
			// subscribe to server stream (Server PLAY)
			} else if (word == "splay") {
				std::string stream;
				ss >> stream;
				glirr->play(stream);
			// Server STOP
			} else if (word == "sstop") {
				std::string stream;
				ss >> stream;
				glirr->stop(stream);
			// list streams in 'play' set
			} else if (word == "slistplay") {
				glirr->listplay();
			// list currently playable streams
			} else if (word == "slistcurrent") {
				glirr->listcurrent();
			} else if (word == "sinfo") {
				std::string stream;
				ss >> stream;
				glirr->info(stream);
			} else if (word == "json") {
				std::string json;
				ss >> json;
				glirr->send("\02" + json);
			} else
				std::cout << "Unknown command!" << std::endl;
			read();
		});
	}
};

int main() {
	// the Pulse Audio play thread. Plays sound.
	std::thread thread{[]() {
		pa_sample_spec ss{};
		ss.format = PA_SAMPLE_S16LE;
		ss.rate = 48000;
		ss.channels = 1;

		pa_buffer_attr attr{};
		attr.maxlength = 1280;
		attr.minreq = static_cast<uint32_t>(-1);
		attr.prebuf = static_cast<uint32_t>(-1);
		attr.tlength = 1280;

		int error = 0;
		pa_simple* pulse = pa_simple_new(nullptr, "irr", PA_STREAM_PLAYBACK, nullptr, "playback", &ss, nullptr, &attr, &error);
		assert(pulse);

		while (true) {
			std::vector<char> opus;
			bool play = false;
			{
				std::lock_guard<std::mutex> guard{mutex};
				if (current_stream.size() && opus_pieces[current_stream].count(current_pos)) {
					opus = opus_pieces[current_stream][current_pos++];
					play = true;
				}
			}
			if (play) {
				std::array<int16_t, 960> a;
				if (opus.size())
					decode.decode(opus.data(), opus.size(), a.data(), a.size());
				else
					decode.decode(nullptr, 0, a.data(), a.size());
				assert(pa_simple_write(pulse, a.data(), a.size() * 2, &error) >= 0);
			} else
				usleep(5000);
		}
	}};

	boost::asio::io_service io_service;
	tui t{io_service};

	tcp::resolver resolver{io_service};
	tcp::resolver::query query{"localhost", "6000"};
	tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

	tcp::socket socket{io_service};
	boost::asio::connect(socket, endpoint_iterator);

	irr_client client{std::move(socket)};
	glirr = &client;

	io_service.run();
}
